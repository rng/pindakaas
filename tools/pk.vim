if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syn case match

syn keyword pkKeyword  and or if then else elsif return var
syn keyword pkKeyword  for in do while as const
syn keyword pkDefineK  end
syn keyword pkType     Int Bool Char String Array ByteArray

syn match pkType     "@\h\w*"  display
syn match pkInteger  "\<0[xX]\x\+\%(_\x\+\)*\>"							display
syn match pkInteger  "\<\%(0[dD]\)\=\%(0\|[1-9]\d*\%(_\d\+\)*\)\>"		display
syn match pkConstant "\<[^.][A-Z_][A-Z0-9_]*\>" display

syn match pkComment  "#.*"

syn match pkDeclaration "[^[:space:];#(]\+" contained
syn match pkFunction    "\<[_[:alpha:]][_[:alnum:]]*[?]\=[[:alnum:].:?!=]\@!" contained containedin=pkDeclaration

syn match pkDefine      "\<def\>" nextgroup=pkDeclaration skipwhite skipnl
syn match pkModule      "\<module\>" nextgroup=pkDeclaration skipwhite skipnl
syn match pkStruct      "\<struct\>" nextgroup=pkDeclaration skipwhite skipnl
syn match pkRequire     "\<require\>" nextgroup=pkDeclaration skipwhite skipnl


" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_pk_syntax_inits")
  if version < 508
    let did_pk_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  " The default methods for highlighting.  Can be overridden later
  HiLink pkInteger  Number

  HiLink pkComment  Comment

  HiLink pkConstant Constant

  HiLink pkKeyword  Keyword
  HiLink pkRequire  Define
  HiLink pkModule   Define
  HiLink pkStruct   Define
  HiLink pkDefine   Define
  HiLink pkDefineK  Define
  HiLink pkFunction Function
  HiLink pkType     Type

  delcommand HiLink
endif

let b:current_syntax = "pk"
