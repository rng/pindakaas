#!/bin/sh
arm-elf-objcopy -I binary -O elf32-littlearm -B arm $1 dis.elf 
arm-elf-objcopy --set-section-flags .data=code dis.elf
arm-elf-objdump -d -M force-thumb dis.elf
