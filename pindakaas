#!/usr/bin/env python
# vim:set ts=2 sw=2 sts=2 et: #

import readline
import atexit
from pk.parser import parseRepl
import pk.parser as parser
import pk.gen.codegen as codegen
import pk.remote.funcs as funcs
from pk import comp, typecore, ast, fixedpoint
import argparse
import time
import os

aparser = argparse.ArgumentParser(description="Pindakaas REPL")
aparser.add_argument("files", nargs="*", type=str, help="Source files")
args = aparser.parse_args()
intbase = 10


replhist = os.path.expanduser("~/.pkhist")
try:
  readline.read_history_file(replhist)
except IOError:
  pass
atexit.register(readline.write_history_file, replhist)
readline.parse_and_bind("tab: complete")


def custom_input(prompt, default=""):
  readline.set_startup_hook(lambda: readline.insert_text(default))
  try:
    val = raw_input(prompt)
  finally:
    readline.set_startup_hook(None)
  return val


def autocomplete_lookup(compiler, text, state):
  def modlookup(mod, name, path):
    if "." in name:
      ms = name.split(".")
      if ms[0] not in mod.values.keys():
        return None
      return modlookup(mod.lookup(ms[0])[1], ".".join(ms[1:]), path+[ms[0]])
    allnames = ["require", "def", "quit"] if len(path)==0 else []
    allnames += mod.values.keys()
    return [".".join(path+[x]) for x in allnames if x.startswith(name)]
  comps = modlookup(compiler.fileenv, text, [])
  if state<len(comps):
    return comps[state]
  return None


def countblocks(s):
  bc = 0
  for w in s.split():
    if w in ["def", "then", "do"]:
      bc += 1
    elif w in ["end"]:
      bc -= 1
  return bc


def compileFile(compiler, f, mc):
  try:
    compiler.startfile(f)
    compiler.compileFile(f, mc)
  except (parser.ParserException, typecore.TypeException),e:
    src = file(f).read()
    ast.handle_compile_error(e, src)


def debugCommand(funcman, funstr):
  dcmd = funstr.split()
  if dcmd[0]=="help":
    print "REPL top-level syntax (basic tab completion):"
    print "  def foo ... end"
    print "  require ..."
    print "  foo()"
    print "  quit"
    print ""
    print "Debug commands:"
    print "  /help             - this text"
    print "  /funcs            - list loaded functions"
    print "  /reset            - force target reset"
    print "  /i <base>         - set integer printing base"
    print "  /r <addr> <len>   - read target memory"
    print "  /w <addr> <d0> .. - write target memory"
    print "  /j <addr>         - call function on target"
    print "  /d <functionname> - disassemble function"
    print ""
  elif dcmd[0]=="dump":
    funcman.dump(*dcmd[1:])
  elif dcmd[0]=="funcs":
    funcman.funcinfo()
  elif dcmd[0]=="reset":
    funcman.reset()
  elif dcmd[0]=="b":
    funcman.backtrace()
  elif dcmd[0]=="r":
    if len(dcmd) == 2:
      funcman.read(int(dcmd[1],16),1)
    else:
      funcman.read(int(dcmd[1],16),int(dcmd[2],16))
  elif dcmd[0]=="w":
    funcman.write(int(dcmd[1],16),[int(x,16) for x in dcmd[2:]])
  elif dcmd[0]=="j":
    funcman.jump(int(dcmd[1],16))
  elif dcmd[0]=="d":
    funcman.disassemble(dcmd[1])
  elif dcmd[0]=="i":
    global intbase
    intbase = int(dcmd[1])
  else:
    print "Unknown debug command '%s'" % funstr


def repl():
  funstr = ""
  needmore = False
  funcman = funcs.FuncManager()
  compiler = comp.Compiler(funcman.machine.arch)
  compiler.setMemorySizes(funcman.machine.rambase, funcman.machine.ramsize)
  modcache = {}

  def handleRepl(funstr):
    fn = parseRepl(funstr)
    compiler.cgreset()
    if isinstance(fn, parser.Require):
      tstart = time.time()
      if compiler.fileenv.has(fn.name):
        compiler.fileenv.remove(fn.name)
      # FIXME: in-module relocs probably won't work
      compiler.compileFile(compiler.find_module(fn.name), modcache)
      compiler.generate(doAll=True)
      code, addrpairs = compiler.cg.relocmodule(funcman.varbase)

      fnrs = []
      for fnname, sw, ew, relocs in addrpairs:
        fnrs.append(funcman.push(fnname, code[sw:ew], relocs))
        if fnname in compiler.vectors:
          funcman.vector(fnname, compiler.vectors[fnname])
      for fnr in fnrs:
        fnr.relocate()
      for fnr in fnrs:
        funcman.refreshFunc(fnr.name)
      if len(code):
        print "Loaded in %0.1fs" % (time.time()-tstart)
      else:
        print "Module already loaded"
    else:
      ftype = compiler.compileFunction(fn)
      compiler.generate(doAll=True)
      code,names,relocs = compiler.cg.relocable(funcman.varbase)
      funcman.push(fn.name, code, relocs).relocate()
      funcman.refreshFunc(fn.name)
      if fn.name=="anon":
        ret = funcman.call(fn.name)
        rt = typecore.prune(ftype.types[-1])
        rt = rt.name if isinstance(rt, typecore.TypeOper) else repr(rt)
        if rt == "Int":
          fmt = {10 : "%d", 16 : "0x%08x"}.get(intbase, "%d")
          if intbase == 10 and ret > 0x7FFFFFFF:
              ret = -(ret^0xFFFFFFFF)-1
          print fmt % ret
        elif rt == "Fx16":
          if ret > 0x7FFFFFFF:
              ret = -(ret^0xFFFFFFFF)-1
          print "%.4f" % fixedpoint.fx_to_float(ret)
        elif rt == "Bool":
          print "%s" % (ret==1)
        elif rt == "Char":
          print "'%c'" % chr(ret&0xFF)
        elif rt == "None":
          pass
        else:
          print "%s(0x%x)" % (rt, ret)

  # shoddy auto-complete
  def completer(text, state):
    return autocomplete_lookup(compiler, text, state)
  readline.set_completer(completer)

  for a in args.files:
    compileFile(compiler, a, modcache)

  print "\nPindakaas REPL. Type '/help' for info."
  compiler.startfile("anon")

  while 1:
    bc = countblocks(funstr)
    prompt = ".." if needmore else ">>"
    try:
      l = custom_input(prompt+" ", "  "*bc)
    except KeyboardInterrupt:
      print
      break
    funstr += l+"\n"
    try:
      if l in ["quit", "exit"]:
        break
      if funstr.startswith("/"):
        try:
          debugCommand(funcman, funstr[1:].strip())
        except Exception, e:
          print "Debug command failed: %s" % e
      else:
        handleRepl(funstr)
      needmore = False
      funstr = ""
    except (parser.ParserException, typecore.TypeException, comp.CompilerException),e:
      bc = countblocks(funstr)
      if (e.loc!=len(funstr)) and bc==0:
        if e.fn not in ["", "repl", None]:
            funstr = file(e.fn).read()
        ast.handle_compile_error(e, funstr)
        funstr = ""
        needmore = False
      else:
        needmore = True
    except (codegen.CodegenException, funcs.FuncManException), e:
      print e
      funstr = ""
      needmore = False

repl()
