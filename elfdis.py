#!/usr/bin/env python
# vim:set ts=2 sw=2 sts=2 et: #

from pk.elf import ElfFile
from pk.dis.armv7 import DisassemblerARMv7
import struct
import sys

e = ElfFile(sys.argv[1])
d = DisassemblerARMv7()

code = e.progbits[".text"]
relocs = e.relocs[".rel.text"]

for name, ofs, size, info in e.syms:
  if not (name == "" or name.startswith("$")):
    if size > 0:
      # correct for thumb offset and word align
      ofs = ofs-1
      if size%4 != 0: 
        size += 2
      # unpack code & disassemble
      fcode = code[ofs:ofs+size]
      fcode =  struct.unpack("%dI" % (len(fcode)/4), fcode)
      print name
      d.disassemble(fcode, addr=ofs)
      print

print "RELOCATIONS:"
for ofs, typ, sym, _ in relocs:
  print "%3x %d %s" % (ofs, typ, e.syms[sym][0])

