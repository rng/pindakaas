require Fatal

module Math

  # Absolute value
  def abs(v : Int) : Int
    if v>0 then
      return v
    else
      return -v
    end
  end

  # Modulo 
  def _mod(dividend : Int, divisor : Int) : Int
    # Division by repeated-subtraction with divisor-mutliply speedups
    # Needed for divisionless architectures (ARMv<7)
    # don't do c-style sign correct mod
    dividend = abs(dividend)
    divisor = abs(divisor)
    if divisor == 0 then
      __fatal(3)
    end
    var remainder = dividend
    var next_multiple = divisor

    # calculate largest multiple of divisor that's smaller than the dividend
    var done = false
    var multiple = 0
    while not done do
      multiple = next_multiple
      next_multiple = multiple << 1
      if next_multiple > remainder or next_multiple <= multiple then
        done = true
      end
    end

    # subtract the multiple then shift right
    while multiple >= divisor do
      if multiple <= remainder then
        remainder = remainder - multiple
      end
      multiple = multiple >> 1
    end
    return remainder
  end
end
