#!/usr/bin/env python

import glob
import sys
from pk import ast, reg, typecore, comp, parser
from pk.remote import funcs
import pk.gen.codegen as gen
import time

compiler = comp.Compiler()
funcman = funcs.FuncManager()
compiler.setMemorySizes(funcman.machine.rambase, funcman.machine.ramsize)

class RunException(Exception):
  pass

def compileFile(f):
  try:
    funcman.clear()
    t1 = time.clock()
    compiler.compileFile(f)
    compiler.generate(doAll=True)
      
    code, addrpairs = compiler.cg.relocmodule(funcman.varbase)
    fnrs = []
    for fnname, sw, ew, relocs in addrpairs:
      fnrs.append(funcman.push(fnname, code[sw:ew], relocs))
    for fnr in fnrs:
      fnr.relocate()
    for fnr in fnrs:
      funcman.refreshFunc(fnr.name)

    t2 = time.clock()
    res = funcman.call("main", excp=True)
    t3 = time.clock()
    print "%15s    %0.2f     %0.2f" % (f,t2-t1, t3-t2)
    if res!=0:
      raise RunException("run failed: %d" % res, None)
  except (parser.ParserException, typecore.TypeException, comp.CompilerException), e:
    fn = e.fn if e.fn else f
    funstr = file(fn).read()
    return False, ast.handle_compile_error(e, funstr, printit=False), 0, 0
  except (reg.RegException, gen.CodegenException, funcs.FuncManException, RunException), e:
    return False, str(e), 0, 0
  return True, "ok", t3-t1, len(code)

if len(sys.argv)>1:
  tests = sys.argv[1:]
else:
  tests = sorted(glob.glob("test/*.pk"))

passcount = totalcount = 0
ttotal = wcount = 0
print " Test              comp (s) run (s)"
for f in tests:
  compiler.startfile(f)
  ok, err, t, wordcount = compileFile(f)
  ttotal += t
  wcount += wordcount
  if ok:
    passcount += 1
  else:
    print "###### %s ######" % (f)
    print err
    print
  totalcount += 1
print "%2d/%02d Tests Passed, %0.1fs, %d total words" % (passcount, totalcount, ttotal, wcount)
