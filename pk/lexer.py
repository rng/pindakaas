import ply.lex as lex
from ast import ParserException

def lex_find_column(input, token):
    last_cr = input.rfind('\n', 0, token.lexpos)
    if last_cr < 0: last_cr = 0
    return (token.lexpos - last_cr) + 1

keywords = "module struct def cdef end while if then else elsif return for in do true false require const pass var and or as not".split()

ops = {
    "PLUS" : r"\+",
    "MINUS" : r"-",
    "TIMES" : r"\*",
    "DIVIDE" : r"/",
    "MOD" : r"%",
    "BITAND" : r"&",
    "BITOR" : r"\|",
    "LT" : r"<",
    "GT" : r">",
    "LTE" : r"<=",
    "GTE" : r">=",
    "EQ" : r"==",
    "NE" : r"!=",
    "SHL" : r"<<",
    "SHR" : r">>",
    "ASSIGN" : r"="
}

tokens = [k.upper() for k in keywords]+ops.keys()+[
    "FLOAT", "INT", "CHAR", "NAME", "COMMENT", "STRING", "PRAGMA",
]

for o in ops:
    globals()["t_"+o] = ops[o]

literals = ".,:()[]@"

def t_NAME(t):
    r"[a-zA-Z_][\w_\?]*"
    if t.value in keywords:
        t.type = t.value.upper()
    return t

def t_FLOAT(t):
    r"[0-9]+\.[0-9]+"
    return t

def t_INT(t):
    r"(0x)?[0-9a-fA-F]+"
    return t

def t_CHAR(t):
    r"'\\?.'"
    return t

def t_STRING(t):
    r'\"([^\\\n]|(\\.))*?\"'
    return t

def t_PRAGMA(t):
    r"\#{.*}"
    return t

def t_COMMENT(t):
    r"\#.*"
    pass

def t_newline(t):
    r"\n+"
    t.lexer.lineno += len(t.value)

t_ignore = " \t"

def t_error(t):
    row = t.lexer.lineno
    col = lex_find_column(t.lexer.lexdata, t)-1
    raise ParserException("Invalid character '%s'" % t.value[0], t.lexer.lexpos, row, col, "")

lexerRepl = lex.lex()
lexerFull = lex.lex()
