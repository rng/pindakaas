class TypeException(Exception):
  def __init__(self, msg, expr=None, fn=None):
    Exception.__init__(self, msg)
    self.loc = expr.pos if expr else None
    self.lineno = self.col = None
    self.expr = expr
    self.fn = fn


class TypeOper(object):
  def __init__(self, name, types=None):
    self.name = name
    self.types = types if types else []

  def __repr__(self):
    if len(self.types)==0:
      return self.name
    elif self.name=="Fn":
      return "{0}({1}) -> {2}".format(self.name,
          ', '.join(map(str,self.types[:-1])),
          self.types[-1])
    else:
      return "{0}({1})".format(self.name, ', '.join(map(str,self.types)))


class TypeRecord(TypeOper):
  def __init__(self, name, types, names):
    TypeOper.__init__(self, name, types)
    self.names = names
    def isref(t):
      return t.name not in ["Int", "Char", "Bool"]
    self.numrefs = sum(map(isref, types))

    self.fields = {}
    countr = countn = 0
    for i, name in enumerate(names):
      if isref(types[i]):
        self.fields[name] = (countr, types[i])
        countr += 1
      else:
        self.fields[name] = (countn+self.numrefs, types[i])
        countn += 1

  def header(self):
    return self.numrefs

  def fieldInfo(self, fname):
    return self.fields[fname]


class TypeVar(object):
  count = 0
  def __init__(self):
    self.instance = None
    self.id = TypeVar.count
    TypeVar.count += 1
    self.__name = None

  next_name = 0

  def _getName(self):
    if self.__name is None:
      self.__name = unichr(TypeVar.next_name+0x3b1)
      TypeVar.next_name = TypeVar.next_name+1
    return self.__name

  def __repr__(self):
    if self.instance:
      return str(self.instance)
    if not self.__name:
      self._getName()
    return "%s" % (self.__name.encode("utf-8"))


class BaseTypes:
  T_Int = TypeOper("Int")
  T_Fx16 = TypeOper("Fx16")
  T_Char = TypeOper("Char")
  T_Bool = TypeOper("Bool")
  T_None = TypeOper("None")
  T_String = TypeOper("String")
  T_Module = TypeOper("Module")


def occursIn(t, types):
  return any(occursInType(t,t2) for t2 in types)


def occursInType(v, type2):
  pruned_type2 = prune(type2)
  if pruned_type2 == v:
    return True
  elif isinstance(pruned_type2, TypeOper):
    return occursIn(v, pruned_type2.types)
  return False


def prune(t):
  if isinstance(t, TypeVar):
    if t.instance is not None:
      t.instance = prune(t.instance)
      return t.instance
  return t


def isGeneric(v, nongen):
  return not occursIn(v, nongen)


def fresh(t, nongen):
  mappings = {}
  def freshrec(tp):
    p = prune(tp)
    if isinstance(p, TypeVar):
      if isGeneric(p, nongen):
        if p not in mappings:
          mappings[p] = TypeVar()
        return mappings[p]
      else:
        return p
    elif isinstance(p, TypeRecord):
      return TypeRecord(p.name, [freshrec(x) for x in p.types], p.names)
    elif isinstance(p, TypeOper):
      return TypeOper(p.name, [freshrec(x) for x in p.types])
    else:
      raise TypeException("fresh")
  return freshrec(t)


def isPolymorphic(t):
  if isinstance(t,TypeVar) and not t.instance:
    return True
  elif isinstance(t,TypeVar):
    return isPolymorphic(t.instance)
  else:
    for tt in t.types:
      if isPolymorphic(tt):
        return True
    return False


def boxingFunctionFor(t):
  if isinstance(t, TypeVar) and t.instance:
    return boxingFunctionFor(t.instance)
  elif isinstance(t, TypeVar):
    return "box" # FIXME: Needed? can't just assume value will already be boxed?
  elif isinstance(t, TypeOper):
    bf = {
      "Int" : "i",
      "Bool" : "b",
      "Char" : "c",
      "Array" : None,
      "ByteArray" : None,
      "String" : None
    }
    if isinstance(t, TypeRecord):
      return None
    if t.name not in bf:
        raise TypeException("Cannot box type '%s'" % t)
    v = bf[t.name]
    if v:
      return "box"+v
    return None

class TypeState:
  def __init__(self, filename, modname=""):
    self.fn = filename
    self.modname = modname
    self.func()
    self.types = {
        "Int" : BaseTypes.T_Int,
        "Bool" : BaseTypes.T_Bool,
        "Char" : BaseTypes.T_Char,
        "Fx16" : BaseTypes.T_Fx16,
        "Array" : TypeOper("Array"),
        "ByteArray" : TypeOper("ByteArray"),
        "String" : BaseTypes.T_String,
    }
    self.modtypes = {}

  def type_from_ast(self, var, expr, typevarsAllowed=False):
    def type_from_name(name, args):
      if name.startswith("@"):
        if not typevarsAllowed:
          raise TypeException("Type variable not allowed", expr, self.fn)
        if name[1:] in self.fntypes:
          return self.fntypes[name[1:]]
        tv = TypeVar()
        self.fntypes[name[1:]] = tv
        return tv
      elif name in ["Array", "ByteArray"]:
        if len(args)!=1:
          raise TypeException("Invalid array type", expr, self.fn)
        return TypeOper(name, map(lambda x: type_from_name(x.name, x.args), args))
      elif name in self.types:
        return self.types[name]
      elif name in self.fntypes:
        return self.fntypes[name]
      elif name in self.modtypes:
        return self.modtypes[name]
      else:
        raise TypeException("Unknown type '%s'" % name, expr, self.fn)
    return type_from_name(var.name, var.args)

  def check_type(self, tname, expr=None):
    if tname not in self.types and tname not in self.modtypes:
      raise TypeException("Unknown type '%s'" % tname, expr, self.fn)

  def func(self):
    self.calls = 0
    self.fntypes = {}


