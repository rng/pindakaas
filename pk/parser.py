import ply.yacc as yacc
from ast import (ParserException, Module, Struct, Require, Function,
                 CFunction, Pragma, Arg, Type, Block, Pass, Const, Return,
                 While, If, For, Assign, Op, Unary, Call, Var, As, String,
                 Float, Int, Bool, Char, ByteArray, Tuple)
from lexer import lexerRepl, lexerFull, lex_find_column
from lexer import tokens

precedence = (
        ("left", "OR"),
        ("left", "AND"),
        ("left", "BITOR"),
        ("left", "BITAND"),
        ("left", "LT", "EQ", "GT", "LTE", "GTE", "NE"),
        ("left", "SHR", "SHL"),
        ("left", "PLUS", "MINUS"),
        ("left", "TIMES", "DIVIDE", "MOD"),
)

# return a rule function for a sequence (possibly delimited) of elements
def make_sequence_rule(name, elt, delim=""):
    def fn(p):
        spacing = 0 if delim=="" else 1
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == (3+spacing):
            p[1].append(p[2+spacing])
            p[0] = p[1]
        else:
            raise Exception("bad sequence length: %d, %s" % (len(p), p[:]))
    fn.__doc__ = """%s : %s %s %s
                       | %s""" % (name, name, delim, elt, elt)
    return fn

# return a rule function for an optional value
def make_opt_rule(name, empty):
    def fn(p):
        p[0] = p[1] if p[1] else empty
    fn.__doc__ = """%s_opt : %s
                           | empty""" % (name, name)
    return fn

def make(t, *args):
  return t._make(args)

def p_programtop(p):
    """programtop : program"""
    p[0] = p[1]

p_program = make_sequence_rule("program", "programelt")

def p_repl(p):
    """repl : funcdecl
            | callstmt
            | expr
            | require"""
    p[0] = p[1]

def p_programelt(p):
    """programelt : funcdecl
                  | moddecl
                  | structdecl
                  | conststmt
                  | require"""
    p[0] = p[1]

def p_moddecl(p):
    """moddecl : MODULE NAME modelts_opt END"""
    p[0] = make(Module, p[2], p[3], "unknown", p.lexpos(1))

p_modelts_opt = make_opt_rule("modelts", [])

p_modelts = make_sequence_rule("modelts", "modelt")

def p_modelt(p):
    """modelt : funcdecl
              | cfuncdecl
              | structdecl
              | funcarg
              | conststmt"""
    p[0] = p[1]

def p_structdecl(p):
    """structdecl : STRUCT NAME structelts_opt END"""
    p[0] = make(Struct, p[2], p[3], p.lexpos(1))

p_structelts_opt = make_opt_rule("structelts", [])

p_structelts = make_sequence_rule("structelts", "funcarg")

def p_require(p):
    """require : REQUIRE NAME"""
    p[0] = make(Require, p[2], p.lexpos(1))

def p_funcdecl(p):
    """funcdecl : pragma_opt DEF NAME '(' funcargs_opt ')' rtype_opt block_opt END"""
    p[0] = make(Function, p[3], p[5], p[7], p[8], p[1], p.lexpos(2))

def p_cfuncdecl(p):
    """cfuncdecl : pragma_opt CDEF string NAME '(' funcargs_opt ')' rtype_opt"""
    p[0] = make(CFunction, p[4], p[6], p[8], p[3], p[1], p.lexpos(2))

p_pragma_opt = make_opt_rule("pragma", None)

def p_pragma(p):
    """pragma : PRAGMA"""
    p[0] = make(Pragma, p[1][2:-1].split(","), p.lexpos(1))

p_rtype_opt = make_opt_rule("rtype", None)

def p_rtype(p):
    """rtype : ':' typename"""
    p[0] = p[2]

p_funcargs_opt = make_opt_rule("funcargs", [])

p_funcargs = make_sequence_rule("funcargs", "funcarg", "','")

def p_funcarg(p):
    """funcarg : NAME ':' typename"""
    p[0] = make(Arg, p[3], p[1], p.lexpos(1))

def p_typename(p):
    """typename : NAME
                | NAME '[' typename ']'
                | '@' NAME """
    if len(p) == 2:
        p[0] = make(Type, p[1], [], p.lexpos(1))
    elif len(p) == 3:
        p[0] = make(Type, "@"+p[2], [], p.lexpos(1))
    elif len(p) == 5:
        p[0] = make(Type, p[1], [p[3]], p.lexpos(1))
    else:
        raise Exception("Complex typename")

p_block_opt = make_opt_rule("block", Block([]))

def p_block(p):
    """block : block_helper"""
    p[0] = make(Block, p[1])

p_block_helper = make_sequence_rule("block_helper", "stmt")

def p_stmt(p):
    """stmt : assignstmt 
            | varstmt
            | ifstmt 
            | forstmt 
            | whilestmt 
            | returnstmt 
            | conststmt 
            | callstmt
            | passstmt"""
    p[0] = p[1]

def p_callstmt(p):
    """callstmt : postfix_expr '(' ')' 
                | postfix_expr '(' expr_list ')'"""
    if len(p) == 4:
        p[0] = make(Call, p[1], [], p.lexpos(1))
    elif len(p) == 5:
        p[0] = make(Call, p[1], p[3], p.lexpos(1))
    else:
        raise  Exception()

def p_passstmt(p):
    """passstmt : PASS"""
    p[0] = Pass(p.lexpos(1))

def p_conststmt(p):
    """conststmt : CONST NAME ASSIGN operand"""
    p[0] = make(Const, p[2], p[4], p.lexpos(1))

def p_returnstmt(p):
    """returnstmt : RETURN expr"""
    p[0] = make(Return, p[2], p.lexpos(1))

def p_whilestmt(p):
    """whilestmt : WHILE expr DO block_opt END"""
    p[0] = make(While, p[2], p[4], p.lexpos(1))

def p_forstmt(p):
    """forstmt : FOR NAME IN expr DO block_opt END"""
    p[0] = make(For, p[2], p[4], p[6], p.lexpos(1))

def p_ifstmt(p):
    """ifstmt : IF expr THEN block elsifs_opt else_opt END"""
    # convert elsifs into nested if-elses
    fb = p[6]
    for eif in p[5][::-1]:
      ei = make(If, eif[0], eif[1], fb, p.lexpos(1))
      fb = make(Block, [ei])
    p[0] = make(If, p[2], p[4], fb, p.lexpos(1))

p_elseifs_opt = make_opt_rule("elsifs", [])

p_elsifs = make_sequence_rule("elsifs", "elsif")

def p_elsif(p):
    """elsif : ELSIF expr THEN block"""
    p[0] = (p[2], p[4])

p_else_opt = make_opt_rule("else", [])

def p_else(p):
    """else : ELSE block"""
    p[0] = p[2]

def p_varstmt(p):
    """varstmt : VAR varname ASSIGN expr"""
    p[0] = make(Assign, p[2], p[4], True, p.lexpos(1))

def p_assignstmt(p):
    """assignstmt : varname ASSIGN expr"""
    p[0] = make(Assign, p[1], p[3], False, p.lexpos(1))

def p_expr(p):
    """expr : expr PLUS expr
            | expr MINUS expr
            | expr TIMES expr
            | expr DIVIDE expr
            | expr MOD expr
            | expr AND expr
            | expr OR expr
            | expr BITAND expr
            | expr BITOR expr
            | expr EQ expr
            | expr NE expr
            | expr LT expr
            | expr GT expr
            | expr LTE expr
            | expr GTE expr
            | expr SHL expr
            | expr SHR expr
            | '(' expr ')'
            | unary_expr"""
    if len(p)==4:
        if p[1] == '(':
            p[0] = p[2]
        else:
            p[0] = make(Op, p[2], p[1], p[3], p.lexpos(2))
    else:
        p[0] = p[1]

def p_unary_expr(p):
    """unary_expr : call_expr
                  | NOT unary_expr
                  | MINUS unary_expr"""
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 3:
        p[0] = make(Unary, p[1], p[2], p.lexpos(1))

def p_call_expr(p):
    """call_expr : postfix_expr
                 | call_expr '(' ')'
                 | call_expr '(' expr_list ')' """
    if len(p) == 2:
        p[0] = p[1]
    else:
        if len(p) == 4:
            p[0] = make(Call, p[1], [], p.lexpos(1))
        else:
            p[0] = make(Call, p[1], p[3], p.lexpos(1))

def p_postfix_expr(p):
    """postfix_expr : operand
                    | call_expr AS typename
                    | call_expr '.' field_list"""
    if len(p) == 2:
        p[0] = p[1]
    else:
        if p[2] == '.':
            assert len(p[3]) == 1
            p[0] = make(Var, p[3][0], [p[1].value], p.lexpos(2))
        else:
            p[0] = make(As, p[1], p[3], p.lexpos(2))

def p_operand(p):
    """operand : int
               | float
               | char
               | string
               | varname 
               | tuple
               | bytearray
               | boolean"""
    p[0] = p[1]

def p_tuple(p):
    """tuple : '(' expr_list ')'"""
    p[0] = make(Tuple, p[2], p.lexpos(2))

def p_bytearray(p):
    """bytearray : '[' BITOR operand_list BITOR ']'"""
    p[0] = make(ByteArray, p[3], p.lexpos(3))

p_operand_list = make_sequence_rule("operand_list", "operand", "','")

p_expr_list = make_sequence_rule("expr_list", "expr", "','")

p_field_list = make_sequence_rule("field_list", "NAME", "'.'")

def p_string(p):
    """string : STRING"""
    c = p[1][1:-1]
    p[0] = make(String, c, p.lexpos(1))

def p_char(p):
    """char : CHAR"""
    c = p[1][1:-1]
    if c[0] =="\\":
      charmap = {"n":"\n", "r":"\r", "t":"\t", "'":"'"}
      if c[1] in charmap:
        c = charmap[c[1]]
      else:
        raise ParserException("Bad escape char %s" % c, p.lexpos(1), p.lineno(1), 0, "")
    p[0] = make(Char, c, p.lexpos(1))

def p_int(p):
    """int : INT"""
    try:
      if p[1].startswith("0x"):
        p[0] = make(Int, int(p[1][2:],16), p.lexpos(1))
      else:
        p[0] = make(Int, int(p[1]), p.lexpos(1))
    except ValueError:
      raise ParserException("Invalid integer: %s" % p[1], p.lexpos(1), p.lineno(1), 0, "")

def p_float(p):
    """float : FLOAT"""
    p[0] = make(Float, float(p[1]), p.lexpos(1))

def p_boolean(p):
    """boolean : TRUE
               | FALSE"""
    p[0] = make(Bool, p[1] == "true", p.lexpos(1))

def p_varname(p):
    """varname : NAME"""
    p[0] = make(Var, p[1], [], p.lexpos(1))

def p_empty(p):
    'empty : '
    pass

# Error rule for syntax errors
def p_error(p):
    if not p:
        raise ParserException("Unexpected end of file", 0, 0, 0, "")
    col = lex_find_column(p.lexer.lexdata, p)
    #row = p.lineno
    raise ParserException("Syntax error", p.lexpos, p.lineno, col, "")

def parseProgram(s, modname="", fn=""):
  lexerFull.lineno = 1
  parser = yacc.yacc(tabmodule="fulltab", start="programtop")
  try:
    return make(Module, modname, parser.parse(s, lexer=lexerFull), fn, 0)
  except ParserException,e:
    raise ParserException(e, e.loc, e.lineno, e.col, fn)

def parseRepl(s):
  lexerRepl.lineno = 1
  parser = yacc.yacc(tabmodule="repltab", start="repl")
  r = parser.parse(s, lexer=lexerRepl)
  if isinstance(r, (Function, Require)):
    return r
  else:
    rt = make(Type, "@anon", [], 0)
    return make(Function, "anon", [], rt, make(Block,[make(Return,r,0)]), [], 0)
