# vim:set ts=2 sw=2 sts=2 et: #
from disassembler import Disassembler

class DisassemblerARMv7(Disassembler):

  def __init__(self, func_lookup=None):
    self.func_lookup = func_lookup if func_lookup else lambda x: None

  def dis16_arith(self, inst):
    return self.match("16arith", inst, (9,), [
      ("000xx", "lsl   {r3,0}, {r3,3}, #{i5,6,0}"),
      ("001xx", "lsr   {r3,0}, {r3,3}, #{i5,6,0}"),
      ("010xx", "asr   {r3,0}, {r3,3}, #{i5,6,0}"),
      ("01100", "add   {r3,0}, {r3,3}, {r3,6}"),
      ("01110", "add   {r3,0}, {r3,3}, #{i3,6,0}"),
      ("01111", "sub   {r3,0}, {r3,3}, #{i3,6,0}"),
      ("01101", "sub   {r3,0}, {r3,3}, {r3,6}"),
      ("100xx", "mov   {r3,8}, #{i8,0,0}"),
      ("101xx", "cmp   {r3,8}, #{i8,0,0}"),
      ("111xx", "sub   {r3,8}, #{i8,0,0}"),
    ])

  def dis16_dproc(self, inst):
    return self.match("16dproc", inst, (6,), [
      ("0000", "and   {r3,0}, {r3,0}, {r3,3}"),
      ("1010", "cmp   {r3,0}, {r3,3}"),
      ("1100", "orr   {r3,0}, {r3,0}, {r3,3}"),
      ("1101", "mul   {r3,0}, {r3,0}, {r3,3}"),
      ("1001", "rsb   {r3,0}, {r3,3}"),
    ])

  def dis16_special(self, inst):
    return self.match("16special", inst, (6,), [
      ("00xx", "add   {r3,0}, {r3,0}, {r3,3}"),
      ("10xx", "mov   {m3,0,7}, {r4,3}"),
      ("110x", "bx    {r4,3}"),
    ])

  def dis16_ldst(self, inst):
    return self.match("16ldst", inst, (9,), [
      ("01100xx", "str   {r3,0}, [{r3,3}, #{i5,6,2}]"),
      ("01101xx", "ldr   {r3,0}, [{r3,3}, #{i5,6,2}]"),
      ("10010xx", "str   {r3,8}, [sp, #{i8,0,2}]"),
      ("10011xx", "ldr   {r3,8}, [sp, #{i8,0,2}]"),
    ])

  def dis16_nop(self, inst):
    return self.match("16nop", inst, (4, 0), [
      ("xxxx", "^0000", "it{c4}  #{i4,0,0}"),
      ("0000", "0000",  "nop"),
      ("0011", "0000",  "wfi"),
    ])

  def dis16_cps(self, inst):
    return self.match("16cps", inst, (0,), [
      ("00010", "cpsie i"),
      ("10010", "cpsid i"),
    ])

  def dis16_misc(self, inst):
    return self.match("16misc", inst, (5,), [
      ("00000xx", "add   sp, #{i7,0,2}"),
      ("00001xx", "sub   sp, #{i7,0,2}"),
      ("00x1xxx", "cbz   {r3,0}, {b5}"),
      ("010xxxx", "push  {l0}"),
      ("10x1xxx", "cbnz  {r3,0}, {b5}"),
      ("110xxxx", "pop   {l1}"),
      ("1110xxx", "bkpt  {i8,0,0}"),
      ("0110011", self.dis16_cps),
      ("1111xxx", self.dis16_nop),
    ])

  def dis16_condb(self, inst):
    return self.match("16condb", inst, (8,), [
      ("^111x",  "b{c8}   {b4}"),
    ])

  def dis16(self, inst):
    return self.match("", inst, (10,), [
      ("00xxxx", self.dis16_arith),
      ("010000", self.dis16_dproc),
      ("010001", self.dis16_special),
      ("01001x", "ldr   {r3,8}, [pc, #{i8,0,2}]"),
      ("0101xx", self.dis16_ldst),
      ("011xxx", self.dis16_ldst),
      ("100xxx", self.dis16_ldst),
      ("10100x", "adr   {r3,8}, [pc, #{i8,0,2}]"),
      ("10101x", "add   {r3,8}, sp, #{i8,0,2}"),
      ("1011xx", self.dis16_misc),
      #("11000x", ".stm"),
      #("11001x", ".ldm"),
      ("1101xx", self.dis16_condb),
      ("11100x", "b     {b3}"),
    ])

  def dis32_dproc_shr(self, inst):
    return self.match("32dprocshr", inst, (21,16), [
      ("0000", "^1111", "and   {r4,8}, {r4,16}, {r4,0}, {s}"),
      ("0001", "xxxx",  "bic   {r4,8}, {r4,16}, {r4,0}, {s}"),
      ("0010", "^1111", "orr   {r4,8}, {r4,16}, {r4,0}, {s}"),
      ("0010", "1111",  "mov   {r4,8}, {r4,0}, {s}"),
      ("0011", "^1111", "orn   {r4,8}, {r4,16}, {r4,0}, {s}"),
      ("0100", "^1111", "eor   {r4,8}, {r4,16}, {r4,0}, {s}"),
      ("1000", "^1111", "add   {r4,8}, {r4,16}, {r4,0}, {s}"),
      ("1010", "xxxx",  "adc   {r4,8}, {r4,16}, {r4,0}, {s}"),
      ("1011", "xxxx",  "sbc   {r4,8}, {r4,16}, {r4,0}, {s}"),
      ("1101", "^1111", "sub   {r4,8}, {r4,16}, {r4,0}, {s}"),
      ("1110", "xxxx",  "rsb   {r4,8}, {r4,16}, {r4,0}, {s}"),
    ])

  def dis32_dproc_mi(self, inst):
    return self.match("32dprocmi", inst, (20,16,8), [
      ("0010x", "1111", "xxxx",  "mov   {r4,8}, #{t}"),
      ("0100x", "xxxx", "^1111", "eor   {r4,8}, {r4,16}, #{t}"),
      ("1000x", "xxxx", "^1111", "add   {r4,8}, {r4,16}, #{t}"),
      ("1101x", "xxxx", "1111",  "cmp   {r4,16}, #{t}"),
    ])

  def dis32_dproc_i(self, inst):
    return self.match("32dproci", inst, (20,), [
      ("00000", "add   {r4,8}, {r4,16}, #{h12}"),
      ("00100", "mov   {r4,8}, #{h16}"),
      ("01010", "sub   {r4,8}, {r4,16}, #{h12}"),
      ("01100", "movt  {r4,8}, #{h16}"),
    ])

  def dis32_branch(self, inst):
    return self.match("32branch", inst, (12,20), [
      ("0x0", "xxxxxxx", "b{c22}   {b1}"),
      ("0x1", "xxxxxxx", "b     {b0}"),
      ("1x1", "xxxxxxx", "bl    {b2}"),
    ])

  def dis32_st(self, inst):
    return self.match("32st", inst, (21,6), [
      ("110", "xxxxxx", "str   {r4,12}, [{r4,16}, #{i12,0,0}]"),
      ("010", "1xxxxx", "str   (i)"),
      ("010", "0xxxxx", "str   (r)"),
    ])

  def dis32_ldw(self, inst):
    return self.match("32ldw", inst, (23,6,16), [
      ("01", "xxxxxx", "^1111", "ldr   {r4,12}, [{r4,16}, #{i12,0,0}]"),
      ("00", "1xx1xx", "^1111", "ldr   {r4,12}, [{r4,16}, ???]"),
      ("00", "1100xx", "^1111", "ldr   {r4,12}, [{r4,16}, ???]"),
      ("00", "1110xx", "^1111", "ldrt"),
      ("00", "000000", "^1111", "ldr   (r)"),
      ("0x", "xxxxxx", "1111",  "ldr   (l)"),
    ])

  def dis32_dproc_r(self, inst):
    return self.match("32dprocr", inst, (20, 4), [
      ("000x", "0000", "lsl   {r4,8}, {r4,16}, {r4,0}"),
      ("001x", "0000", "lsr   {r4,8}, {r4,16}, {r4,0}"),
    ])

  def dis32_lmul(self, inst):
    return self.match("32lmul", inst, (20, 4), [
      ("001", "1111", "sdiv  {r4,8}, {r4,16}, {r4,0}"),
      ("011", "1111", "udiv  {r4,8}, {r4,16}, {r4,0}"),
      ("000", "0000", "smull {r4,12}, {r4,8}, {r4,16}, {r4,0}"),
    ])

  def dis32_vfp_sp_dproc(self, inst):
    return self.match("32vfpsp_dproc", inst, (20,6), [
      ("0x11","0", "fadds {v12}, {v16}, {v0}"),
      ("0x11","1", "fsubs {v12}, {v16}, {v0}"),
    ])

  def dis32_vfp_sp_ldst(self, inst):
    #"PUdWL"
    return self.match("32vfpsp_ldst", inst, (20,), [
      ("01x10", "fstms {v12}, [{r4,16}, #{i8,0,2}]"),
      ("01x11", "fldmias {r4,16}!, {v12}, #{i8,0,0}"), # fixme
      ("11x00", "fsts  {v12}, [{r4,16}, #{i8,0,2}]"),
      ("11x01", "flds  {v12}, [{r4,16}, #{i8,0,2}]"),
      ("xxxxx", "vfplds? {i5,20,0}"),
    ])

  def dis32_vfp_sp(self, inst):
    return self.match("32vfpsp", inst, (24,4), [
      ("1110", "001", "vfp   ??? (reg)"),
      ("1110", "xx0", self.dis32_vfp_sp_dproc),
      ("110x", "xxx", self.dis32_vfp_sp_ldst),
    ])

  def dis32_copr(self, inst):
    return self.match("32copr", inst, (8,), [
      ("1010", self.dis32_vfp_sp),
    ])

  def dis32(self, inst):
    return self.match("", inst, (20,15), [
      #("0100xx0xx", "x", ".ldmstm"),
      #("0100xx1xx", "x", ".lddstd"),
      ("0101xxxxx", "x", self.dis32_dproc_shr),
      ("011xxxxxx", "x", self.dis32_copr),
      ("10x0xxxxx", "0", self.dis32_dproc_mi),
      ("10x1xxxxx", "0", self.dis32_dproc_i),
      ("10xxxxxxx", "1", self.dis32_branch),
      ("11000xxx0", "x", self.dis32_st),
      #("1100xx001", "x", ".ldb"),
      #("1100xx011", "x", ".ldh"),
      ("1100xx101", "x", self.dis32_ldw),
      ("11010xxxx", "x", self.dis32_dproc_r),
      ("110110xxx", "x", "mul   {r4,8}, {r4,16}, {r4,0}"),
      ("110111xxx", "x", self.dis32_lmul),
    ])

  def disassemble(self, code, addr=0, markpc=-1, names={}):
    self.tcode = []
    for i in code:
      self.tcode.append(i&0xFFFF)
      self.tcode.append((i>>16)&0xFFFF)
    pc = 0
    assembly = []
    while pc<len(self.tcode):
      self.pc = addr+(pc*2)
      i = self.tcode[pc]
      if (i&0xf800) in [0xf800, 0xf000, 0xe800]:
        i = (i<<16) | self.tcode[pc+1]
        inst = "%08x  %s" % (i, self.dis32(i))
        pc += 2
      else:
        inst = "%04x      %s" % (i, self.dis16(i))
        pc += 1
      assembly.append((self.pc, inst))
    # if we have a specific pc of interest, only print around that region
    if markpc>0:
      for i,v in enumerate(assembly):
        if v[0] == markpc:
          assembly = assembly[max(i-4,0):i+5]
          break
    # print disassembly
    for pc,i in assembly:
      if pc in names:
        print "%s:" % names[pc]
      print (">" if pc == markpc else " "),
      print "%08x: %s" % (pc, i)
