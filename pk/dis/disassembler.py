# vim:set ts=2 sw=2 sts=2 et: #
class Disassembler:

  def mask_sized(self, size):
    return (1<<size)-1

  def check_match(self, data, match):
    revmatch = False
    if match[0] == "^":
      revmatch = True
      match = match[1:]
    for i,b in enumerate(match[::-1]):
      mask = 1<<i
      if b in "x_":
        continue
      elif b == "1":
        if data&mask == 0:
          return revmatch
      elif b == "0":
        if data&mask != 0:
          return revmatch
    return not revmatch

  def _p(self, inst, fmt):
    def f(inst, ofs, size):
      return (inst>>ofs)&((1<<size)-1)

    regnames = "r0 r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 r11 r12 sp lr pc".split()
    vfpregnames = ["s%d" % r for r in range(32)]
    ccodes = "eq ne cs cc mi pl vs vc hi ls ge lt ge le al".split()
    import re
    def inst_rep(p):
      args = p.group()[2:-1]
      v = map(int,args.split(",")) if len(args) else []
      o = p.group()[1]
      if o == "b":
        # branch: bx
        imm11,j2,j1 = f(inst,0,11), f(inst,11,1), f(inst,13,1)
        imm10,imm6,sign = f(inst,16,10),f(inst,16,6),f(inst,26,1)
        if v[0]==1:
          # 19bit offset
          bv = (j2<<19)|(j1<<18)|(imm6<<12)|(imm11<<1)
          if sign: bv |= (0xFFF00000)
        elif v[0] in [0,2]:
          # 23bit offset
          i1 = 1-(j1^sign)
          i2 = 1-(j2^sign)
          bv = (i1<<23)|(i2<<22)|(imm10<<12)|(imm11<<1)
          if sign: bv |= (0xFF000000)
        elif v[0]==3:
          # 11bit offset
          bv = f(inst,0,10)<<1
          sign = f(inst,10,1)
          if sign: bv |= (0xFFFFF800)
        elif v[0]==4:
          # 8bit offset
          bv = f(inst,0,7)<<1
          sign = f(inst,7,1)
          if sign: bv |= (0xFFFFFF00)
        elif v[0]==5:
          # 6bit offset
          bv = (f(inst,3,5) | (f(inst,9,1)<<5)) << 1
          sign = 0
        if sign:
          bv = -(bv^0xFFFFFFFF)-1
        # lookup function if doing a bl
        fnaddr = self.pc + bv + 4
        if v[0] == 2:
          fn = self.func_lookup(fnaddr)
          if fn:
            return "%s (%x)" % (fn, fnaddr)
        # otherwise return plain address
        return "%x" % (fnaddr)
      if o == "c":
        # condition code: cx
        return ccodes[f(inst, v[0], 4)]
      if o == "r":
        # reg: mx,y
        return regnames[f(inst, v[1], v[0])]
      if o == "m":
        # reg with extra bit: mx,y,z
        return regnames[f(inst, v[1], v[0])+(f(inst, v[2], 1)<<3)]
      if o == "v":
        # vfp reg: vx
        extrabit = {0 : 5, 12 : 22, 16 : 7 }[v[0]] # vfp regs have an extra bit
        return vfpregnames[(f(inst, v[0], 4)<<1)+f(inst, extrabit, 1)]
      if o == "l":
        # reglist: lx
        regs = []
        rl = f(inst, 0, 8)
        for i in range(8):
          if rl&(1<<i):
            regs.append(regnames[i])
        if f(inst,8,1):
          regs.append("pc" if v[0] else "lr")
        return ", ".join(regs)
      if o == "h":
        # split immediate: hx
        iv = f(inst,0,8)|(f(inst,12,3)<<8)|(f(inst,26,1)<<11)
        if v[0] == 12:
            return "%x" % iv
        elif v[0]== 16:
            return "%x" % (iv|(f(inst,16,4)<<12))
      if o == "i":
        # immediate: ix,y,z
        return "%x" % (f(inst, v[1], v[0])<<v[2])
      if o == "s":
        # shift
        stype = f(inst, 4, 2)
        simm = f(inst, 6, 2)|(f(inst, 12, 3)<<2)
        stname = ["lsl", "lsr", "asr", "ror"][stype]
        if simm == 0:
          return ""
        return "%s %d" % (stname, simm)
      if o == "t":
        # thumb 2 immediate
        imm4, imm8 = ((f(inst,26,1)<<3)|f(inst,12,3)), f(inst,0,8)
        if (imm4>>2)==0:
          imm2 = imm4&3
          if imm2 == 0:
            return "%x" % imm8
          elif imm2 == 1:
            return "%x" % ((imm8<<16)|(imm8))
          elif imm2 == 2:
            return "%x" % ((imm8<<24)|(imm8<<8))
          else:
            return "%x" % ((imm8<<24)|(imm8<<16)|(imm8<<8)|(imm8))
        else:
          shift = 32- ((imm4 << 1)|(imm8 >> 7))
          return "%x" % ((imm8|0x80) << shift)
      return "[???]"

    return re.sub("{.*?}", inst_rep, fmt)

  def match(self, name, inst, offsets, rules):
    for rule in rules:
      matches, action = rule[:-1], rule[-1]
      ok = True
      for i, match in enumerate(matches):
        data = (inst>>offsets[i]) & self.mask_sized(len(match))
        if not self.check_match(data, match):
          ok = False
          break
      if ok:
        if isinstance(action, str):
          return self._p(inst, action)
        else:
          return action(inst)
    return "? %s (0x%x)" % (name, inst)
