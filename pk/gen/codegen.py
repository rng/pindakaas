# vim:set ts=2 sw=2 sts=2 et: #
from pk.reg import reg

class CodegenException(Exception):
  pass

class Codegen(object):
  def __init__(self, doVectors=False):
    self.reset()

  def startmod(self):
    self.modvars = {}
    self.modvarcount = 0

  def startfn(self, funcname, frame, clobbers, leaf):
    self.funcname = funcname
    self.clobbers = clobbers
    self.frame = frame
    self.spills, self.heaps, self.callargs = (
      frame.count("spill"), frame.count("heap"), frame.count("callarg"))
    self.label(funcname)
    self.names[funcname] = self.labels[funcname]
    self.lits = []
    self.lithash = {}
    self.litvalues = {}
    self.leaf = leaf

  def endfn(self):
    raise CodegenException("unimplemented endfn")

  def addVar(self, name):
    if name not in self.modvars:
      self.modvars[name] = self.modvarcount
      self.modvarcount += 1
    #self.branches.append((len(self.code), name, "var"))
    return self.modvars[name]

  def addLit(self, value, label=None):
    name = "_%s_lit%d" % (self.funcname, len(self.lits))
    litstart = len(self.lits)
    if isinstance(value, (str, list)):
      # object header
      wordsize = 1+(((len(value)&0xFFFC)+4)>>2)
      self.lits.extend([wordsize, 16])
      # byte lit size
      self.lits.extend([len(value), 0])
      if isinstance(value, str):
          value = map(ord, value)
      if len(value)%2 == 1:
        value.append(0)
      for i in range(len(value)/2):
        self.lits.append((value[i*2+1]<<8)+value[i*2])
    else:
      if value in self.litvalues:
        return self.litvalues[value]
      if not label:
        self.litvalues[value] = name
      self.lits.append(value&0xFFFF)
      self.lits.append(value>>16)
    self.lithash[name] = (litstart*2, label)
    return name

  def prolog(self):
    self.clobbers -= set(['r8'])
    framelen = self.spills + self.heaps + self.callargs
    # leaf functions can use ARM link register instead of push/pop
    if not (self.leaf and len(self.clobbers) == 0):
      # push clobbers
      self.push(self.clobbers)
      framelen += len(self.clobbers) + 1
    # add stack
    self.addstack(self.spills + self.heaps + self.callargs + 1)
    # put stack info into first slot
    stackinfo = (framelen<<8) | ((self.heaps+self.callargs) << 16) | (0x42)
    self.integer(reg.r(7), stackinfo, litok=False)
    self.storestack(reg.r(7), -1)
    # zero heap slots
    if self.heaps > 0:
      self.integer(reg.r(7), 0)
      for i in range(self.heaps):
        self.storestack(reg.r(7), i)

  def postret(self):
    raise CodegenException("unimplemented postret")

  def setvarbase(self, vb):
    raise CodegenException("unimplemented varbase")

  def ret(self):
    self.addstack( -(self.spills + self.heaps + self.callargs + 1))
    # leaf function does a link return otherwise pop
    if self.leaf and len(self.clobbers) == 0:
      self.retleaf()
    else:
      self.pop(self.clobbers)
    self.postret()

  def call(self, fn):
    if fn == "__mem_peek":
      self.load(reg.r(0), reg.r(0), 0)
    elif fn == "__mem_poke":
      self.store(reg.r(1), reg.r(0), 0)
    elif fn == "__mem_peekb":
      self.loadbyte(reg.r(0), reg.r(0), 0)
    elif fn == "__mem_pokeb":
      self.storebyte(reg.r(1), reg.r(0), 0)
    elif fn == "__get_sp":
      self.mov(reg.r(0), reg.r(13))
    elif fn == "__intr_enable":
      self.cps(True, 1, 0)
    elif fn == "__intr_disable":
      self.cps(False, 1, 0)
    elif fn == "__wfi":
      self.wfi()
    else:
      self.funccall(fn)

  def reset(self):
    self.code = []
    self.labels = {}
    self.branches = []
    self.names = {}
    self.startmod()
    self.postreset()

  def postreset(self):
    pass

  def label(self, name):
    raise CodegenException("unimplemented label")
