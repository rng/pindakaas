# vim:set ts=2 sw=2 sts=2 et: #
"""
ARMv7 Codegen

Comments of the form A6.x.xxx are references to the ARMv7-M Architecture
Application Level Reference Manual (DDI 0405B)
"""

from codegen import CodegenException
from arm import CodegenARM, ccodes
from pk.utils import bit_extract

def gen_modimm(v):
  b3, b2, b1, b0 = bit_extract(v, (8,24), (8,16), (8,8), (8,0))
  if b3==b2==b1==0:
    return 0, 0, b0
  if b3==b1==0 and b2==b0:
    return 0, 1, b0
  if b2==b0==0 and b3==b1:
    return 0, 2, b1
  if b3==b2==b1==b0:
    return 0, 3, b0
  for i in range(32):
    if v&(1<<i):
      lb = i
      break
  for i in range(32):
    if v&(1<<(31-i)):
      hb = 31-i
      break
  if hb-lb>8:
    raise CodegenException("can't modimm %08X" % v)
  sv = (31-hb)+8
  a = sv&1
  imm3 = (sv>>1)&7
  i = (sv>>4)&1
  imm8 = (v>>(hb-7))&0x7F|(a<<7)
  return i, imm3, imm8

def gen_imm16(v):
  return bit_extract(v, (4,12), (1,11), (3,8), (8,0))

class CodegenARMv7(CodegenARM):
  archnum = 2

  def make_branch(self, base, label, btype, condition=None):
    # if the branch is backwards and in range, use 16bit encoding
    if label in self.labels:
      ofs = (self.labels[label] - (len(self.code)*2) - 4) >> 1
      if condition is None and -1023<ofs<1024 and btype=="armrel1":
        if ofs<0:
          ofs = (-ofs-1)^0xFFFFFFFF
        self.inst16(0xE000|(ofs&0x7ff))
        return
    # otherwise store for relocation later
    self.branches.append((len(self.code)*2, label, btype))
    self.inst32(base)

  def arith_reg(self, base, rd, rl, rr):
    self.inst32(base|(rl.num()<<16)|(rd.num()<<8)|(rr.num()))

  def arith_reg3216(self, base32, base16, rd, rl, rr):
    if rd.num() == rl.num():
      self.inst16(base16|(rr.num()<<3)|(rd.num()))
    elif rd.num() == rr.num():
      self.inst16(base16|(rl.num()<<3)|(rd.num()))
    else:
      self.arith_reg(base32, rd, rl, rr)

  def arith_imm12(self, base, rd, rl, intimm):
    assert 0 <= intimm < 4096
    _, imm1, imm3, imm8 = gen_imm16(intimm)
    c = base|(rd.num()<<8)|(rl.num()<<16)|(imm1<<26)|(imm3<<12)|imm8
    self.inst32(c)

  def arith3216_imm5(self, base32, base16, rd, rl, intimm):
    if intimm < 32:
      self.inst16(base16|(rd.num())|(rl.num()<<3)|(intimm<<6))
    else:
      imm3 = (intimm>>2)&7
      imm2 = (intimm)&3
      self.inst32(base32|(imm3<<12)|(rd.num()<<8)|(imm2<<6)|(rl.num()))

  def arith_imm3(self, base, rd, rl, intimm):
    self.inst16(base|(rd.num())|(rl.num()<<3)|(intimm<<6))

  def nop16(self):
    # A6.7.87 (T1) NOP
    self.inst16(0xBF00)

  def load(self, rd, rb, imm):
    # A6.7.42 (T1) LDR (imm)
    imm >>= 2
    assert imm < 32
    self.inst16(0x6800|(imm<<6)|(rb.num()<<3)|rd.num())

  def store(self, rd, rb, imm):
    # A6.7.118 (T1) STR (imm)
    imm >>= 2
    assert imm < 32
    self.inst16(0x6000|(imm<<6)|(rb.num()<<3)|rd.num())

  def loadbyte(self, rd, rb, imm):
    # A6.7.45 (T1) LDRB (imm)
    assert imm < 32
    self.inst16(0x7800|(imm<<6)|(rb.num()<<3)|rd.num())

  def storebyte(self, rd, rb, imm):
    # A6.7.120 (T1) STRB (imm)
    assert imm < 32
    self.inst16(0x7000|(imm<<6)|(rb.num()<<3)|rd.num())

  def cps(self, enable, pri, fault):
    # B3.1.1 CPS
    self.inst16(0xB660|((not enable)<<4)|(pri<<1)|(fault<<0))

  def wfi(self):
    # A6.7.152 WFI
    self.inst16(0xBF30)

  def breakpoint(self, n):
    # A6.7.17 BKPT
    self.inst16(0xBE00|n)

  def retleaf(self):
    self.inst16(0x4700|(14<<3))

  def funccall(self, fn):
    if fn=="__mem_call0":
      # A6.7.19 BLX r0
      self.inst16(0x4780)
    else:
      # A6.7.18 BL
      self.make_branch(0xF000D000, fn, "armrelfn")

  def branchif(self, op, label):
    # A6.7.12 (T3) B
    cc = ccodes[op]
    self.make_branch(0xF0008000|(cc<<22), label, "armrel2", condition=cc)

  def branch(self, label):
    # A6.7.12 (T4) B
    self.make_branch(0xF0009000, label, "armrel1")

  def mov(self, rd, rs):
    if rd.num()<8 and rs.num()<16:
      # A6.7.76 (T1) MOV (reg)
      self.inst16(0x4600|(rs.num()<<3)|(rd.num()))
    else:
      # A6.7.76 (T3) MOV (reg)
      self.inst32(0xEA4F0000|(rd.num()<<8)|rs.num())

  def arith(self, op, rd, rl, rr, intimm):
    if rr:
      if op=="&" or op=="and":
        # A6.7.9 (T2) AND (reg)
        self.arith_reg3216(0xEA000000, 0x4000, rd, rl, rr)
      elif op=="|" or op=="or":
        # A6.7.91 (T1/T2) ORR (reg)
        self.arith_reg3216(0xEA400000, 0x4300, rd, rl, rr)
      elif op=="-":
        # A6.7.132 (T2) SUB (reg)
        self.arith_reg(0xEBA00000, rd, rl, rr)
      elif op=="+":
        # A6.7.4 (T3) ADD (reg)
        self.arith_reg3216(0xEB000000, 0x4400, rd, rl, rr)
      elif op=="*":
        # A6.7.83 (T2) MUL (reg)
        self.arith_reg3216(0xFB00F000, 0x4340, rd, rl, rr)
      elif op=="/":
        # A6.7.110 (T1) SDIV (reg)
        self.arith_reg(0xFB90F0F0, rd, rl, rr)
      elif op=="<<":
        # A6.7.68 (T1/T2) LSL (reg)
        self.arith_reg3216(0xFA00F000, 0x4080, rd, rl, rr)
      elif op==">>":
        # A6.7.70 (T1/T2) LSR (reg)
        self.arith_reg3216(0xFA20F000, 0x40C0, rd, rl, rr)
      else:
        raise CodegenException("unhandled arith reg %s" % op)
    else:
      if op=="+":
        # A6.7.3 (T1/T4) ADD (imm)
        if intimm<8:
          self.arith_imm3(0x1C00, rd, rl, intimm)
        else:
          self.arith_imm12(0xF2000000, rd, rl, intimm)
      elif op=="-":
        # A6.7.131 (T1/T4) SUB (imm)
        if intimm<8:
          self.arith_imm3(0x1E00, rd, rl, intimm)
        else:
          self.arith_imm12(0xF2A00000, rd, rl, intimm)
      elif op=="*":
        raise CodegenException("Multiply immediate not supported in ARM")
      elif op=="/":
        raise CodegenException("Divide immediate not supported in ARM")
      elif op=="&":
        # A6.7.8 (T1) AND (imm)
        self.arith_imm12(0xF0000000, rd, rl, intimm)
      elif op=="<<":
        if intimm==0: # FIXME: optimise this out
          self.mov(rd, rl)
        else:
          # A6.7.67 (T1/T2) LSL (imm)
          self.arith3216_imm5(0xEA4F0000, 0x0000, rd, rl, intimm)
      elif op==">>":
        if intimm==0: # FIXME: optimise this out
          self.mov(rd, rl)
        else:
          # A6.7.69 (T1/T2) LSR (imm)
          self.arith3216_imm5(0xEA4F0010, 0x0800, rd, rl, intimm)
      else:
        raise CodegenException("unhandled arith imm %s" % op)

  def cmp(self, op, rl, rr, intimm):
    if rr:
      # A6.7.28 (T1&T3) CMP (reg)
      if rl.num()<8 and rr.num()<8:
        self.inst16(0x4280|(rr.num()<<3)|(rl.num()))
      else:
        self.inst32(0xEBB00F00|(rl.num()<<16)|(rr.num()))
    else:
      # A6.7.27 (T1&T3) CMP (imm)
      if rl.num()<8 and intimm<0xFF:
        self.inst16(0x2800|(rl.num()<<8)|(intimm))
      else:
        imm1, imm3, imm8 = gen_modimm(intimm)
        self.inst32(0xF1B00F00|(rl.num()<<16)|(imm1<<26)|(imm3<<12)|imm8)

  def integer(self, r, value, litok=True):
    if value<0:
      # FIXME: stupid handling of negative numbers
      self.integer(r, -value)
      self.negate(r,r)
    elif value<=0xFF:
      # A6.7.75 (T1) MOV (imm)
      self.inst16(0x2000|(r.num()<<8)|value)
    elif value<=0xFFFF:
      # A6.7.75 (T3) MOV (imm)
      imm4, imm1, imm3, imm8 = gen_imm16(value)
      c = 0xF2400000|(imm1<<26)|(imm4<<16)|(imm3<<12)|(r.num()<<8)|(imm8)
      self.inst32(c)
    elif value<0xFFFFFFFF:
      # FIXME: need to work out something with signed vs unsigned values
      if litok:
        litname = self.addLit(value)
        self.branches.append((len(self.code)*2, litname, "pcrel16"))
        # A6.7.43 (T1) LDR (lit)
        self.inst16(0x4800|(r.num()<<8))
      else:
        # A6.7.78 (T1) MOVT
        self.integer(r, value&0xFFFF)
        value = value>>16
        imm4, imm1, imm3, imm8 = gen_imm16(value)
        c = 0xF2C00000|(imm1<<26)|(imm4<<16)|(imm3<<12)|(r.num()<<8)|(imm8)
        self.inst32(c)
    else:
      raise CodegenException("Integer literal too large: 0x%08x" % value)

  def bytearray(self, r, value):
    litname = self.addLit(value)
    self.branches.append((len(self.code)*2, litname, "pcrel16"))
    # A6.7.7 (T1) ADR
    self.inst16(0xA000|(r.num()<<8))

  def negate(self, rd, rs):
    # A6.7.105 (T1) RSB (imm)
    self.inst16(0x4240|(rs.num()<<3)|(rd.num()))

  def boolnot(self, rd, rs):
    # A6.7.34 EOR (imm)
    self.inst32(0xF0800001|(rs.num()<<16)|(rd.num()<<8))

  def storestack(self, rs, loc):
    # A6.7.118 (T2) STR (imm)
    self.inst16(0x9000|(rs.num()<<8)|(loc+1))

  def loadstack(self, rd, loc):
    # A6.7.42 (T2) LDR (imm)
    self.inst16(0x9800|(rd.num()<<8)|(loc+1))

  def setvarbase(self, rd):
    litname = self.addLit(0, "_varbase")
    self.branches.append((len(self.code)*2, litname, "pcrel16"))
    # A6.7.43 (T1) LDR (lit)
    self.inst16(0x4800|(rd.num()<<8))

  def storevar(self, name, rs, rb):
    self.store(rs, rb, self.addVar(name)*4)

  def loadvar(self, rd, rb, name):
    self.load(rd, rb, self.addVar(name)*4)

  def it(self, op, mask):
    # A6.7.37 (T1) IT
    self.inst16(0xBF00|(ccodes[op]<<4)|mask)

  def setif(self, op, rd):
    # see Table A6-3:
    # ITE mask = (not firstcond[0]) 1 0 0
    mask = 4 | ((not (ccodes[op]&1)) << 3)
    self.it(op, mask)
    self.integer(rd, 1)
    self.integer(rd, 0)

  def _smull(self, rdlo, rdhi, rl, rr):
    self.inst32(0xFB800000|(rl.num()<<16)|(rdlo.num()<<12)|(rdhi.num()<<8)|rr.num())

  def patch(self, addr, label, btype):
    laddr = self.labels[label]
    ofs = (laddr - addr) - 4
    if ofs<0:
      ofs = (-ofs-1)^0xFFFFFFFF
    if btype == "armrel1" or btype=="armrelfn":
      imm11, imm10, i2, i1, s = bit_extract(ofs, (11,1), (10,12), (1,22), (1,23), (1,24))
      j1 = 1 - (i1^s)
      j2 = 1 - (i2^s)
      self.code[addr/2+1] |= (j1<<13)|(j2<<11)|(imm11)
      self.code[addr/2]   |= (s<<10)|(imm10)
    elif btype == "armrel2":
      imm11, imm6, i2, i1, s = bit_extract(ofs, (11,1), (6,12), (1,18), (1,19), (1,20))
      self.code[addr/2+1] |= (i1<<13)|(i2<<11)|(imm11)
      self.code[addr/2]   |= (s<<10)|(imm6)
    elif btype == "pcrel16":
      # pc relative needs a 4-byte aligned offset
      addralign = addr&0xFFFFFFFC
      ofs = (laddr - addralign) - 4
      self.code[addr/2] |= ((ofs)>>2)
    elif btype == "literal":
      self.code[addr/2+1] = (laddr>>16)&0xFFFF
      self.code[addr/2] = laddr&0xFFFF
    else:
      raise CodegenException("Unhandled branch type '%s'" % btype)

  def relocable(self, modvarbase):
    self.labels["_varbase"] = modvarbase
    crossfnrelocs = ["armrelfn", "thumbrelfn"]
    # patch only relocs that are local to functions
    for addr, label, btype  in self.branches:
      if btype not in crossfnrelocs:
        self.patch(addr, label, btype)
    # get code
    code = []
    if len(self.code)%2 == 1:
      raise CodegenException("Code not word aligned")
    for i in range(len(self.code)/2):
      code.append((self.code[i*2+1]<<16)|(self.code[i*2]))
    # return code with any cross-function relocs
    return code, self.names, [b for b in self.branches if b[2] in crossfnrelocs]

  def relocmodule(self, modvarbase):
    code,names,relocs = self.relocable(modvarbase)

    addrs = sorted([(a,n) for n,a in names.items()])
    onames = addrs + [(len(code)<<2,"")]
    frelocs = dict([(n,[]) for n in names])
    for raddr, rname, rtype in relocs:
      ok = False
      for i in range(len(onames)-1):
        if raddr>=onames[i][0] and raddr<onames[i+1][0]:
          fname = onames[i][1]
          frelocs[fname].append((raddr-onames[i][0], rname, rtype))
          ok = True
          break
      if not ok:
        raise CodegenException("can't relocate %s: %04x" % (rname, raddr))

    funcinfo = []
    for i, fnpair in enumerate(addrs):
      nextaddr = len(code)*4 if i == (len(addrs)-1) else addrs[i+1][0]
      fnstart, fnname = fnpair
      sw, ew = (fnstart >> 2), (nextaddr >> 2)
      funcinfo.append((fnname, sw, ew, frelocs[fnname]))

    return code, funcinfo

  def finish(self, modvarbase, vectors):
    self.labels["_varbase"] = modvarbase
    if self.doVectors:
      maddr = self.labels["main"]+1
      self.code[2] |= maddr&0xFFFF
      self.code[3] |= maddr>>16
      for vectname, vectnum in vectors.items():
        vectaddr = self.labels[vectname]+1
        self.code[(vectnum*2)  ] = vectaddr&0xFFFF
        self.code[(vectnum*2)+1] = vectaddr>>16
    # patch
    for addr, label, btype  in self.branches:
      self.patch(addr, label, btype)

    return self.code, self.names, self.modvarcount
