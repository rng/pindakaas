# vim:set ts=2 sw=2 sts=2 et: #
from codegen import Codegen

ccodes = {
  "==" : 0,
  "!=" : 1,
  ">=" : 10,
  "<"  : 11,
  ">"  : 12,
  "<=" : 13,
}

class CodegenARM(Codegen):
  def __init__(self, doVectors=False):
    self.doVectors = doVectors
    Codegen.__init__(self)

  def postreset(self):
    if self.doVectors:
      # Vector table:
      # 0x00: stack start
      # 0x04: reset addr,
      # 0x08: exception addr --.
      # ....                   |
      # 0xNN: b .; nop       <-'
      vectorCount = 60
      nopOffset = ((2+vectorCount)*4 + 1) << 16 # offset of nop int handler
      for v in [0x30002000, 0] + ([nopOffset]*vectorCount) + [0xe7febf00]:
        self.inst32(v)

  def inst16(self, i):
    self.code.append(i)

  def inst32(self, i):
    self.code.append(i>>16)
    self.code.append(i&0xFFFF)

  def cfunc(self, funcname, code, relocs):
    self.funcname = funcname
    self.label(funcname)
    self.names[funcname] = self.labels[funcname]
    # add relocations from elf, adjusting with current code length
    for ofs, typ, label in relocs:
      assert typ == 102
      self.branches.append((ofs+(len(self.code)*2), label, "thumbrelfn"))
    # add code from elf
    for c in code:
      self.code.append(c & 0xFFFF)
      self.code.append(c >> 16)

  def push(self, regs):
    v = 0
    for c in regs:
      v |= (1<<int(c[1:]))
    self.inst16(0xB500|v)

  def pop(self, regs):
    v = 0
    for c in regs:
      v |= (1<<int(c[1:]))
    self.inst16(0xBD00|v)

  def addstack(self, n):
    if n>0:
      self.inst16(0xB080|n) # sub sp
    else:
      self.inst16(0xB000|(-n)) # add sp

  def postret(self):
    pass

  def postfn(self):
    if len(self.code)%2 == 1:
      self.nop16()

  def label(self, name):
    self.labels[name] = len(self.code)*2

  def setuparg(self, n, r):
    self.mov(n, r)

  def endfn(self):
    if len(self.code)%2 == 1:
      self.nop16()
    # add lits
    for lname in self.lithash:
      litstart, litlabel = self.lithash[lname]
      if litlabel:
        self.branches.append((len(self.code)*2, litlabel, "literal"))
      self.labels[lname] = len(self.code)*2 + litstart
    for l in self.lits:
      self.code.append(l)
    self.postfn()

  def getvarbase(self):
    # FIXME: get this from the machine descriptor
    return 0x20000110
