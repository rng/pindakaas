# vim:set ts=2 sw=2 sts=2 et: #
from ctypes import (Structure, POINTER, cdll, c_uint, c_ubyte, c_ushort, 
                    c_longlong, c_ulonglong, c_void_p, c_int, c_char_p,
                    c_char, byref, cast)
import struct
from collections import namedtuple

EM_ARM    = 40

(SHT_NULL, SHT_PROGBITS, SHT_SYMTAB, SHT_STRTAB, SHT_RELA,
 SHT_HASH, SHT_DYNAMIC,  SHT_NOTE,   SHT_NOBITS, SHT_REL) = range(10)

class Elf64_Shdr(Structure):
  _fields_ = [
    ("sh_name", c_uint),
    ("sh_type", c_uint),
    ("sh_flags", c_ulonglong),
    ("sh_addr", c_ulonglong),
    ("sh_offset", c_ulonglong),
    ("sh_size", c_ulonglong),
    ("sh_link", c_uint),
    ("sh_info", c_uint),
    ("sh_addralign", c_ulonglong),
    ("sh_entsize",   c_ulonglong),
  ]

class Elf64_Sym(Structure):
  _fields_ = [
    ("st_name",  c_uint),
    ("st_info",  c_ubyte),
    ("st_other", c_ubyte),
    ("st_shndx", c_ushort),
    ("st_value", c_ulonglong),
    ("st_size",  c_ulonglong),
  ]

class Elf64_Rela(Structure):
  _fields_ = [
    ("r_offset", c_ulonglong),
    ("r_info",   c_ulonglong),
    ("r_addend", c_longlong),
  ]

class Elf64_Rel(Structure):
  _fields_ = [
    ("r_offset", c_ulonglong),
    ("r_info",   c_ulonglong),
  ]

def ELF64_R_SYM(i): return ((i) >> 32)
def ELF64_R_TYPE(i): return ((i) & 0xffffffff)

class Elf_Data(Structure):
  _fields_ = [
    ("d_buf",     c_void_p),
    ("d_type",    c_uint),
    ("d_version", c_int),
    ("d_size",    c_ulonglong),
    ("d_off",     c_ulonglong),
    ("d_align",   c_ulonglong),
  ]

Elf_Ehdr = namedtuple("Elf64_Ehdr", """
e_ident e_type e_machine e_version e_entry e_phoff e_shoff
e_flags e_ehsize e_phentsize e_phnum e_shentsize e_shnum e_shstrndx """)

libelf = cdll.LoadLibrary("libelf.so")

class ElfFile:
  def __init__(self, fn):
    f = file(fn, "rw")
    data = f.read()
    e_machine = struct.unpack_from("H", data, 18)[0]
    if e_machine == EM_ARM:
      hdr_fmt = "16sHHIIIIIHHHHHH"
    else:
      raise Exception("unhandled machine %d" % e_machine)
    elf_header = Elf_Ehdr._make(struct.unpack_from(hdr_fmt, data))
    fd = f.fileno() 

    libelf.elf_strptr.restype = c_char_p
    libelf.elf_getdata.restype = POINTER(Elf_Data)

    libelf.elf_version(1)

    self.elf = libelf.elf_begin(fd, 1, None)
    scn = libelf.elf_nextscn(self.elf, None)

    self.progbits = {}
    self.relocs = {}
    self.syms = []
    while scn:
      hdr = Elf64_Shdr()
      libelf.gelf_getshdr(scn, byref(hdr))
      edata = libelf.elf_getdata(scn, None)
      sname = libelf.elf_strptr(self.elf, elf_header.e_shstrndx, hdr.sh_name)
      if hdr.sh_type == SHT_PROGBITS:
        self._handle_progbits(hdr, sname, edata)
      elif hdr.sh_type == SHT_SYMTAB:
        self._handle_symtab(hdr, sname, edata)
      elif hdr.sh_type == SHT_REL:
        self._handle_rel(hdr, sname, edata)
      elif hdr.sh_type == SHT_RELA:
        self._handle_rela(hdr, sname, edata)
      else:
        #print "unkonwn elf section", hdr.sh_type
        pass
      scn = libelf.elf_nextscn(self.elf, scn)

  def _handle_progbits(self, hdr, sname, edata):
    if hdr.sh_size>0:
      strdata = cast(edata.contents.d_buf, POINTER(c_char))
      self.progbits[sname] = strdata[:hdr.sh_size]

  def _handle_symtab(self, hdr, sname, edata):
    for i in range (hdr.sh_size / hdr.sh_entsize):
      sym = Elf64_Sym()
      libelf.gelf_getsym(edata, i, byref(sym))
      self.syms.append((
        libelf.elf_strptr(self.elf, hdr.sh_link, sym.st_name), 
        sym.st_value, sym.st_size, sym.st_info))

  def _handle_rel(self, hdr, sname, edata):
    rels = []
    for i in range (hdr.sh_size / hdr.sh_entsize):
      rel = Elf64_Rel()
      libelf.gelf_getrel(edata, i, byref(rel))
      rels.append((
        rel.r_offset, ELF64_R_TYPE(rel.r_info), 
        ELF64_R_SYM(rel.r_info), 0))
    self.relocs[sname] = rels

  def _handle_rela(self, hdr, sname, edata):
    relas = []
    for i in range (hdr.sh_size / hdr.sh_entsize):
      rela = Elf64_Rela()
      libelf.gelf_getrela(edata, i, byref(rela))
      relas.append((
        rela.r_offset, ELF64_R_TYPE(rela.r_info), 
        ELF64_R_SYM(rela.r_info), rela.r_addend))
    self.relocs[sname] = rela

  def getCodeRange(self, symname):
    for name, ofs, size, info in self.syms:
      if name==symname:
        if size > 0:
          # correct for thumb offset and word align
          ofs = ofs-1
          if size%4 != 0: 
            size += 2
          # return code range
          return ofs, (ofs+size)
    raise Exception("Can't find symbol '%s'" % symname)

  def getCodeFor(self, symname):
    s, e = self.getCodeRange(symname)
    # unpack code
    code = self.progbits[".text"]
    fcode = code[s:e]
    return struct.unpack("%dI" % (len(fcode)/4), fcode)

  def getRelocsFor(self, symname):
    s, e = self.getCodeRange(symname)
    rels = []
    # convert relocs to be relative to this symname's start
    for ofs, typ, sym, _ in self.relocs[".rel.text"]:
      if s <= ofs <= e:
        rels.append((ofs-s, typ, self.syms[sym][0]))
    return rels
