# vim:set ts=2 sw=2 sts=2 et: #
from typer import type_module, type_function, TypeState, Environment
from typecore import BaseTypes, TypeOper
from liveness import liveness, CopyPropagate, DeadCode
from gen.armv7 import CodegenARMv7
from reg import reg
from alloc import Allocator
from stack import StackFrame
import elf
import parser
import node
import rtl
import os
import sys
#import time

class CompilerException(Exception):
  def __init__(self, msg, expr=None, fn=None):
    Exception.__init__(self, msg)
    self.loc = expr.pos if expr else None
    self.lineno = self.col = None
    self.expr = expr
    self.fn = fn


class Compiler(object):
  def __init__(self, gen="armv7", doVectors=False, libdirs=None):
    gens = {
      "armv7" : CodegenARMv7
    }
    self.cg = gens[gen](doVectors=doVectors)
    self.reset()
    self.initPaths(libdirs)

  def initPaths(self, libdirs):
    fullpath = sys.argv[0]
    if not fullpath.startswith("/"):
      fullpath = os.path.abspath(fullpath)
    if not os.path.exists(fullpath):
      raise Exception("Can't find full path to pindakaas (%s)" % fullpath)
    self.pkdir = os.path.dirname(fullpath)
    self.libdirs = [os.path.join(self.pkdir, "lib"), "."]
    if libdirs is not None:
      self.libdirs += libdirs

  def find_module(self, name):
    for p in self.libdirs:
      pth = os.path.join(p, "%s.pk" % name.lower())
      if os.path.exists(pth):
        return pth
    return name

  def registerConstInt(self, name, value):
    v = node.Const(name, node.Integer(value))
    self.env.insert(name, BaseTypes.T_Int, v)

  def reset(self):
    self.varbase = self.cg.getvarbase()
    def defprim(name, argtypes, rettype):
      self.env.insert(name, TypeOper("Fn", argtypes+[rettype]), None)

    self.env = Environment("global")
    t_int, t_string, t_none = BaseTypes.T_Int, BaseTypes.T_String, BaseTypes.T_None
    defprim("__mem_peek", [t_int], t_int)
    defprim("__mem_poke", [t_int, t_int], t_int)
    defprim("__mem_peekb", [t_int], t_int)
    defprim("__mem_pokeb", [t_int, t_int], t_int)
    defprim("__mem_call0", [t_int], t_int)
    defprim("__get_sp", [], t_int)
    defprim("__intr_enable", [], t_int)
    defprim("__intr_disable", [], t_int)
    defprim("__wfi", [], t_int)
    self.registerConstInt("_PK_VERSION_NUM_", 0x0011)
    # default values for heap
    self.registerConstInt("_PK_HEAP_BASE_",   0x2000c000)
    self.registerConstInt("_PK_HEAP_SIZE_",   0x2000)
    self.vectors = {}

  def setMemorySizes(self, rambase, ramsize):
    # copying collector, so divide heapspace by 2
    heapsize = (ramsize*0.25)
    #stacksize = 2*1024
    #codesize = ramsize - heapsize*2 - stacksize
    heapbase = rambase+ramsize-(heapsize*2)
    self.registerConstInt("_PK_HEAP_BASE_", heapbase)
    self.registerConstInt("_PK_HEAP_SIZE_", heapsize)

  def startfile(self, fn):
    self.fileenv = Environment("module", parent=self.env)
    self.cgreset()

  def cgreset(self):
    self.cg.reset()
    self.rtls = []

  def _compileFn(self, fn, env, path, filename):
    frame = StackFrame()
    # create cfg
    cfg = fn.cfg(frame)
    cfg.cleanup()
    if cfg.returncheck([]):
      raise CompilerException("Missing return in function '%s'" % fn.name, fn, filename)
    #print "\n%s:" % fn.name
    #print cfg
    #cfg.dot("dot/"+("".join(path)+"_" if len(path) else "")+fn.name)

    # optimise
    done = False
    i = 0
    while not done:
      liveness(cfg)
      opts = [ CopyPropagate, DeadCode ]
      done = not any([o(cfg).run() for o in opts])
      i += 1
      if i>20:
        raise CompilerException("optimisation stuckitude", fn, filename)
    liveness(cfg)

    # linearise and allocate registers
    seq = []
    cfg.linear(seq, [])
    reg.reset()
    seq, clobbers = Allocator().allocate(seq, frame)

    #print "\n","-"*80
    #print "%s:" % fn.name
    #for i,s in enumerate(seq):
    #  print "%2d: %-25s" % (i, s)

    # check clobbered registers
    for c in clobbers:
      if c.startswith("t"):
        raise CompilerException("unhandled temp reg in code")
      if c.startswith("s"):
        raise CompilerException("unhandled spill reg in code")

    # generate
    n_heap, n_spill, n_carg = frame.count("heap"), frame.count("spill"), frame.count("callarg")
    frame.offsets(
      heap  = n_carg,
      spill = n_carg+n_heap,
      arg   = n_carg+n_heap+n_spill+len(clobbers)+2)

    # create RTL
    fpath = "".join(path)+"_" if len(path) else ""
    r=rtl.Module(fpath+fn.name, seq, frame, clobbers) #n_spill, n_heap, clobbers)
    self.rtls.append(r)

    # check if this is a interrupt handler
    if "+vector" in fn.pragmas:
        vect = int(fn.pragmas["+vector"])
        self.vectors[fpath+fn.name] = vect

  def _compileCFn(self, fn, env, path, filename):
    e = elf.ElfFile(fn.cobj.value)
    fpath = "".join(path)+"_" if len(path) else ""
    code, relocs = e.getCodeFor(fn.name), e.getRelocsFor(fn.name)
    relocs = [(ofs, typ, fpath+lbl) for (ofs, typ, lbl) in relocs]
    r = rtl.CModule(fpath+fn.name, code, relocs)
    self.rtls.append(r)

  def compileFunction(self, fn):
    ts = TypeState("repl")
    t,fn = type_function(fn, self.fileenv, ts, set(), "")
    self._compileFn(fn, self.fileenv, [], "repl")
    return t

  def compileProgram(self, prog, filename, modcache):
    def compileModule(modtype, mod, env, path):
      for mt, me in mod.body:
        if isinstance(me, node.Function):
          self._compileFn(me, env, path, filename)
        elif isinstance(me, node.CFunction):
          self._compileCFn(me, env, path, filename)
        elif isinstance(me, (node.ModVar, node.Const, node.Struct)):
          pass
        else:
          compileModule(mt, me, me.env, path+[me.name])
    # handle any requires
    for f in prog.funcs:
      if isinstance(f, parser.Require):
        self.compileFile(self.find_module(f.name), modcache)
    # type and compile module
    modtype, mod = type_module(prog, self.fileenv)
    compileModule(modtype, mod, self.fileenv, [])

  def compileFile(self, filename, modcache=None):
    if not os.path.exists(filename):
      raise CompilerException("can't find module '%s'" % filename)
    # check if we've already loaded this module
    mc = modcache if modcache is not None else {}
    if filename not in mc or mc[filename] < os.path.getmtime(filename):
      src = file(filename).read()
      self.compileProgram(parser.parseProgram(src, fn=filename), filename, mc)
      mc[filename] = os.path.getmtime(filename)

  def generate(self, doAll=False):
    self.cg.startmod()
    # interrupt handlers are often not referenced from any other code, so add
    # them here so we actually generate the code.
    calls = set(["main"]+self.vectors.keys())
    for r in self.rtls:
      calls |= r.calls()
    for r in self.rtls:
      if doAll or (r.name in calls):
        if isinstance(r, rtl.Module):
          # if this function doesn't do any calls, it's a leaf function
          leaf = not any([s.fn for s in r.rtl if isinstance(s, rtl.Call) and
                                              not s.fn.startswith("__")])
          self.cg.startfn(r.name, r.frame, r.clobbers, leaf)
          [s.gen(self.cg) for s in r.rtl]
          self.cg.endfn()
        else:
          self.cg.cfunc(r.name, r.code, r.relocs)

  def code(self):
    self.generate()
    code, names, mvc = self.cg.finish(self.varbase, self.vectors)
    self.varbase += mvc
    return code, names
