import rtl


class BasicBlockVisitor:
  def __init__(self, node):
    self.node = node
    self.changed = False

  def run(self):
    def helper(node, visited):
      if node.name in visited:
        return
      visited.append(node.name)
      self.visit(node)
      for n in node.nexts:
        helper(n, visited)
    helper(self.node, [])
    return self.changed


class BasicBlock:
  count = 0

  def __init__(self, name=""):
    self.name = "%d_%s" % (BasicBlock.count, name)
    BasicBlock.count += 1
    self.statements = []
    self.nexts = []
    self.prevs = []

  def addStatement(self, s):
    self.statements.append(s)

  def addNext(self, n):
    n.addPrev(self)
    self.nexts.append(n)

  def removeNext(self, n):
    self.nexts.pop(self.nexts.index(n))

  def removeNexts(self):
    for n in self.nexts:
      n.removePrev(self)
    self.nexts = []

  def addPrev(self, p):
    self.prevs.append(p)

  def removePrev(self, p):
    self.prevs.pop(self.prevs.index(p))

  def isEntry(self):
    return len(self.prevs)==0

  def isExit(self):
    return len(self.nexts)==0

  def cleanup(self, visited=[]):
    if self.name in visited:
      return
    visited.append(self.name)
    eraseNexts = False
    # add block label
    self.statements.insert(0, rtl.Label(self.name))
    # reset all liveness data
    for s in self.statements:
      s.livein = set()
      s.liveout = set()
    # check if we should erase nexts (if this block has a return)
    if not self.isExit():
      if len(self.statements)>0:
        if isinstance(self.statements[-1], rtl.Return):
          eraseNexts = True
    # try to merge empty branch nodes
    if len(self.statements) == 1 and len(self.nexts) == 1:
      n = self.nexts[0]
      n.removePrev(self)
      for p in self.prevs:
        # for all our previous's, if they end with a jump, fix the label
        if len(p.statements) and isinstance(p.statements[-1], (rtl.Branch, rtl.BranchIf)):
          if p.statements[-1].label == self.name:
            p.statements[-1].label = n.name
        p.removeNext(self)
        p.addNext(n)
    # cleanup nexts (use a copy of the next list, as it could be modified later)
    for n in [x for x in self.nexts]:
      n.cleanup(visited)
    if eraseNexts:
      self.removeNexts()

  def returncheck(self, visited):
    if self.name in visited:
      return False
    visited.append(self.name)
    if (self.isExit() and (not isinstance(self.statements[-1], rtl.Return))): 
      return True
    return any([n.returncheck(visited) for n in self.nexts])

  def all(self, visited):
    if self.name in visited:
      return []
    visited.append(self.name)
    ret = []
    for n in self.nexts:
      ret.extend(n.all(visited))
    ret.append(self)
    return ret

  def linear(self, sequence, visited):
    def add_branch(name):
      sequence.append(rtl.Branch(name))
      sequence[-1].livein = set()
      sequence[-1].liveout = set()

    if self.name in visited:
      return
    visited.append(self.name)
    for s in self.statements:
      if not isinstance(s, rtl.Nop):
        sequence.append(s)
    # if we've already visited a next, add a backwards branch
    if len(self.nexts)==1 and self.nexts[0].name in visited:
      add_branch(self.nexts[0].name)
    # if we're doing a conditional branch that isn't false-first, add a branch
    if len(self.nexts)==2:
      if any([n in self.nexts[0].name for n in ["true","endfor","endwhile"]]):
        add_branch(self.nexts[1].name)
    for n in self.nexts:
      n.linear(sequence, visited)
  
  def dorepr(self, visited):
    if self.name in visited:
      return ""
    visited.append(self.name)
    info = ""
    if self.isEntry(): info += "entry "
    if self.isExit(): info += "exit "
    ret = "(bb %s %s\n" % (self.name, info)
    for s in self.statements:
      ret += "  %s\n" % s
    ret += "  (next %s)\n" % map(lambda x: x.name, self.nexts)
    ret += ")\n"
    for n in self.nexts:
      ret += n.dorepr(visited)
    return ret

  def dot(self, fn):
    import pydot
    g = pydot.Dot()

    def helper(node, visited):
      if node.name in visited:
        return 0
      visited.append(node.name)
      col = "#ffffff"
      if node.isEntry() and node.isExit():
        col = "#ffeeff"
      elif node.isEntry():
        col = "#ffeeee"
      elif node.isExit():
        col = "#eeeeff"
      txt = "\"%s:\\n..\\n%s\"" % (node.name, node.statements[-1])
      g.add_node(pydot.Node(node.name, label=txt, style="filled", fillcolor=col))
      count = 1
      for n in node.nexts:
        count += helper(n, visited)
        g.add_edge(pydot.Edge(node.name, n.name))
      return count 

    if helper(self, []) > 1:
      g.write_png("%s.png" % fn)

  def __repr__(self):
    return self.dorepr([])[:-1]
