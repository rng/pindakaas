# vim:set ts=2 sw=2 sts=2 et: #
from reg import reg

class RTL:
  def regs(self, rd=None, *args):
    self.uregs = [a for a in args if a]
    self.dregs = [rd] if rd else []
    self.rd = rd
    self.rs = self.rl = args[0] if len(args)>0 else None
    self.rr = args[1] if len(args)>1 else None

  def defuses(self):
    return self.uregs+self.dregs

  def cdefs(self):
    return self.dregs

  def cuses(self):
    return self.uregs

  def gen(self, cg):
    raise Exception("no gen for %s" % self)

  def __repr__(self):
    return "  <%s>" % self.reprstr.format(**self.__dict__)


class Prolog(RTL):
  reprstr = "prolog"

  def __init__(self):
    self.regs()

  def gen(self, cg):
    cg.prolog()


class Arith(RTL):
  def __init__(self, op, rd, rl, rr, intimm):
    self.op, self.intimm = op, intimm
    self.regs(rd, rl, rr)

  def __repr__(self):
    r = self.rr if self.rr else self.intimm
    return "  <arith %s %s %s %s>" % (self.op, self.rd, self.rl, r)

  def gen(self, cg):
    cg.arith(self.op, self.rd, self.rl, self.rr, self.intimm)


class SMult64(RTL):
  def __init__(self, rdlo, rdhi, rl, rr):
    # FIXME: RTL with two dest registers is not handled properly
    self.rdlo = rdlo
    self.regs(rdhi, rl, rr)
    self.dregs.append(rdlo)

  def __repr__(self):
    return "  <smult64 %s %s %s %s>" % (self.rdlo, self.rd, self.rl, self.rr)

  def gen(self, cg):
    cg._smull(self.rdlo, self.rd, self.rl, self.rr)


class Unary(RTL):
  reprstr = "unary {op} {rd} {rs}"

  def __init__(self, op, rd, rs):
    self.op = op
    self.regs(rd, rs)

  def gen(self, cg):
    if self.op == "not":
      cg.boolnot(self.rd, self.rs)
    elif self.op == "-":
      cg.negate(self.rd, self.rs)
    else:
      raise Exception("unhandled unary op %s" % (self.op))


class SetIf(RTL):
  reprstr = "setif {rd} {op}"

  def __init__(self, op, rd):
    self.op = op
    self.regs(rd)

  def gen(self, cg):
    cg.setif(self.op, self.rd)


class Cmp(RTL):
  def __init__(self, op, rl, rr, intimm):
    self.op, self.intimm = op, intimm
    self.regs(None, rl, rr)

  def __repr__(self):
    r = self.rr if self.rr else self.intimm
    return "  <cmp %s %s>" % (self.rl, r)

  def gen(self, cg):
    cg.cmp(self.op, self.rl, self.rr, self.intimm)


class Immediate(RTL):
  def __init__(self, value, rd):
    self.value = value
    self.regs(rd)


class Integer(Immediate):
  reprstr = "integer {rd} #{value}"

  def gen(self, cg):
    cg.integer(self.rd, self.value)


class ByteArray(Immediate):
  reprstr = "bytearray {rd} #{value}"

  def gen(self, cg):
    cg.bytearray(self.rd, self.value)


class String(ByteArray):
  reprstr = "string {rd} #'{value}'"
  pass


class LoadStack(RTL):
  reprstr = "ldstack {rd} <- {loc}"

  def __init__(self, rd, loc):
    self.loc = loc
    self.regs(rd)

  def gen(self, cg):
    cg.loadstack(self.rd, self.loc.num())


class StoreStack(RTL):
  reprstr = "ststack {rs} -> {loc}"

  def __init__(self, rs, loc):
    self.loc = loc
    self.regs(None, rs)

  def gen(self, cg):
    cg.storestack(self.rs, self.loc.num())


class Move(RTL):
  reprstr = "move {rd} {rs}"

  def __init__(self, rd, rs):
    self.regs(rd, rs)

  def gen(self, cg):
    cg.mov(self.rd, self.rs)


class LoadLocal(RTL):
  reprstr = "ldlocal {rd} <- {rs}"

  def __init__(self, rs, rd):
    self.regs(rd, rs)

  def gen(self, cg):
    cg.mov(self.rd, self.rs)


class StoreLocal(RTL):
  reprstr = "stlocal {rd} <- {rs}"

  def __init__(self, rd, rs):
    self.regs(rd, rs)

  def gen(self, cg):
    cg.mov(self.rd, self.rs)


class SetupArg(RTL):
  reprstr = "arg {rd} {rs}"

  def __init__(self, n, rs):
    self.regs(reg.r(n), rs)

  def gen(self, cg):
    cg.setuparg(self.rd, self.rs)


class LoadVarBase(RTL):
  reprstr = "varbase {rd}"

  def __init__(self, rd):
    self.regs(rd)

  def gen(self, cg):
    cg.setvarbase(self.rd)


class StoreModVar(RTL):
  reprstr = "stmod {rr}[{mod}.{name}] <- {rs}"

  def __init__(self, rs, rb, mod, name):
    self.regs(None, rs, rb)
    self.mod, self.name = mod, name

  def gen(self, cg):
    cg.storevar("_"+self.mod+"_"+self.name, self.rs, self.rr)


class LoadModVar(RTL):
  reprstr = "ldmod {rd} <- {rs}[{mod}.{name}]"

  def __init__(self, mod, name, rd, rb):
    self.regs(rd, rb)
    self.mod, self.name = mod, name

  def gen(self, cg):
    cg.loadvar(self.rd, self.rs, "_"+self.mod+"_"+self.name)


class StoreRecordVar(RTL):
  reprstr = "strec {rd}.{name} <- {rs}"

  def __init__(self, rs, rd, name):
    self.regs(None, rd, rs)
    self.name = name

  def gen(self, cg):
    cg.store(self.rr, self.rl, (1+self.name)*4)


class LoadRecordVar(RTL):
  reprstr = "ldrec {rd} <- {rs}.{name}"

  def __init__(self, rs, name, rd):
    self.regs(rd, rs)
    self.name = name

  def gen(self, cg):
    cg.load(self.rd, self.rs, (1+self.name)*4)

class Call(RTL):
  reprstr = "call {fn}"

  def __init__(self, fn, nargs):
    self.fn, self.nargs = fn, nargs
    self.regs(reg.r(0), *[reg.r(i) for i in range(nargs)])

  def gen(self, cg):
    cg.call(self.fn)


class Return(RTL):
  reprstr = "return"

  def __init__(self):
    self.regs(None, reg.r(0))

  def gen(self, cg):
    cg.ret()


class BranchIf(RTL):
  reprstr = "branch {op} {label}"

  def __init__(self, label, op, invert=False):
    # invert operation map
    invertmap = {
        "<" : ">=", ">" : "<=",
        "<=" : ">", ">=" : "<",
        "!=" : "==", "==" : "!=",
    }
    if invert: op = invertmap[op]
    self.label, self.op = label, op
    self.regs()

  def gen(self, cg):
    cg.branchif(self.op, self.label)


class Branch(RTL):
  reprstr = "branch {label}"

  def __init__(self, label):
    self.label = label
    self.regs()

  def gen(self, cg):
    cg.branch(self.label)


class Nop(RTL):
  reprstr = "nop"

  def __init__(self):
    self.regs()

  def gen(self, cg):
    cg.nop()


class Label(RTL):
  def __init__(self, name):
    self.name = name
    self.regs()

  def __repr__(self):
    return "%s:" % self.name

  def gen(self, cg):
    cg.label(self.name)


class Module(object):
  def __init__(self, name, rtl, frame, clobbers):
    self.name, self.rtl = name, rtl
    self.frame, self.clobbers = frame, clobbers

  def calls(self):
    c = set()
    for r in self.rtl:
      if isinstance(r, Call):
        c.add(r.fn)
    return c

class CModule(object):
  def __init__(self, name, code, relocs):
    self.name, self.code, self.relocs = name, code, relocs

  def calls(self):
    return set([r[2] for r in self.relocs])
