# Extract fields from a value. Format (width0, ofsset0), .. (widthN, osfsetN)
def bit_extract(v, *fields):
  return [(v >> ofs) & ((1 << width)-1) for width, ofs in fields]
