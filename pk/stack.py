class StackSlot(object):
  def __init__(self, typ, n, frame):
    self.typ, self.n, self.frame = typ, n, frame

  def num(self):
    return self.n

class StackFrame(object):
  def __init__(self):
    self.slots = []

  def allocate(self, typ):
    return self.get(typ, self.count(typ))

  def get(self, typ, num):
    for s in self.slots:
      if s.typ == typ and s.n == num:
        return s
    s = StackSlot(typ, num, self)
    self.slots.append(s)
    return s

  def offsets(self, **ofs):
    for s in self.slots:
      if s.typ in ofs:
        s.n += ofs[s.typ]

  def count(self, typ):
    return len([s for s in self.slots if s.typ == typ])
