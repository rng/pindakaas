import cfg
import rtl
from reg import reg
import typecore
import parser

class Node(object):
  def isCmp(self):
    return False

  def fold(self):
    return self


class Op(Node):
  def __init__(self, op, l, r, typ):
    self.op, self.l, self.r, self.typ = op, l, r, typ

  def isCmp(self):
    return self.op in ["==", "!=", "<", ">", "<=", ">="]

  def applyFold(self, vl, vr):
    import operator
    fns = {
      "+"  : operator.add,
      "-"  : operator.sub,
      "*"  : operator.mul,
      ">>" : operator.rshift,
      "<<" : operator.lshift,
      "==" : operator.eq,
      "!=" : operator.ne,
      ">"  : operator.gt,
      "<"  : operator.lt,
      ">=" : operator.ge,
      "<=" : operator.le,
      "|"  : operator.ior,
      "&"  : operator.iand,
      "/"  : operator.ifloordiv,
      "%"  : operator.imod,
    }
    return fns[self.op](vl, vr)

  def fold(self):
    fl, fr = self.l.fold(), self.r.fold()
    if isinstance(fl, Integer) and isinstance(fr, Integer):
      vl, vr = fl.value, fr.value
      # Don't fold x/0 or x%0 as they cause divide-by-zero
      if not (self.op in ["/", "%"] and vr == 0):
        return Integer(self.applyFold(vl, vr))
    return self

  def reduce(self, l, r):
    import math
    def isPowerTwo(v):
      return v.value > 0 and (v.value & (v.value - 1)) == 0

    if isinstance(r, Integer) and isPowerTwo(r):
      if self.op == "/":
        return Op(">>", l, Integer(int(math.log(r.value, 2))), self.typ)
      elif self.op == "*":
        return Op("<<", l, Integer(int(math.log(r.value, 2))), self.typ)
    elif isinstance(l, Integer) and isPowerTwo(l):
      if self.op == "*":
        return Op("<<", Integer(int(math.log(l.value, 2))), r, self.typ)
    return None

  def immok(self, fr):
    # don't do immediate for multiply/divide/and/or as thumb doesn't support it (FIXME)
    if self.op in ["*", "/", "&", "|", "and", "or"]:
      return False
    if not isinstance(fr, (Integer, Boolean)):
      return False
    # FIXME: make backend specific
    if fr.value > 0xFFF:
      return False
    return True

  def cfg(self, fn, bb, noResult=False):
    # super-simple constant folding
    fv = self.fold()
    if fv != self:
      return fv.cfg(fn, bb)
    # hacky modulo implementation: a%b == a - (a/b)*b (FIXME)
    if self.op == "%":
      stmts = []
      ml, mr = self.l, self.r
      if not isinstance(ml, Integer):
        ml = Var("_vl")
        stmts.append(Assign(ml, self.l))
      if not isinstance(mr, Integer):
        mr = Var("_vr")
        stmts.append(Assign(mr, self.r))
      stmts.append(Op("-", ml, Op("*", mr, Op("/", ml, mr, self.typ), self.typ), self.typ))
      curbb = bb
      for s in stmts:
        curbb, r = s.cfg(fn, curbb)
      return curbb, r
    # check if we can reduce this operation
    fr, fl = self.r.fold(), self.l.fold()
    reduced = self.reduce(fl, fr)
    if reduced:
      return reduced.cfg(fn, bb)
    # check if an immediate value is ok
    if self.immok(fr):
      intimm = int(fr.value)
      rr = None
    else:
      intimm = None
      _, rr = self.r.cfg(fn, bb)
    bbl, rl = self.l.cfg(fn, bb)
    # generate
    if self.isCmp() and noResult:
      rd = None
      bb.addStatement(rtl.Cmp(self.op, rl, rr, intimm))
    elif self.isCmp() and not noResult:
      rd = reg.temp()
      bb.addStatement(rtl.Cmp(self.op, rl, rr, intimm))
      bb.addStatement(rtl.SetIf(self.op, rd))
    else:
      rd = reg.temp()
      # FIXME: identity & comparison of primitive types is broken
      if str(self.typ) == "Fx16" and self.op == "*":
        rdhi, rdlo = reg.temp(), reg.temp()
        bb.addStatement(rtl.SMult64(rdlo, rdhi, rl, rr))
        bb.addStatement(rtl.Arith(">>", rd, rdlo, None, 16))
        bb.addStatement(rtl.Arith("<<", rdhi, rdhi, None, 16))
        bb.addStatement(rtl.Arith("|", rd, rd, rdhi, 0))
      else:
        bb.addStatement(rtl.Arith(self.op, rd, rl, rr, intimm))
    return bb, rd


class Unary(Node):
  def __init__(self, op, expr):
    self.op, self.expr = op, expr

  def fold(self):
    e = self.expr.fold()
    if self.op=="-" and isinstance(e, Integer):
      return Integer(-e.value)
    elif self.op=="-" and isinstance(e, Fixed):
      return Fixed(-e.value)
    elif self.op=="not" and isinstance(e, Boolean):
      return Boolean(not e.value)
    return self

  def cfg(self, fn, bb):
    fv = self.fold()
    if fv != self:
      return fv.cfg(fn, bb)
    _, rs = self.expr.cfg(fn, bb)
    rd = reg.temp()
    bb.addStatement(rtl.Unary(self.op, rd, rs))
    return bb, rd


class Assign(Node):
  def __init__(self, var, expr):
    self.var, self.expr = var, expr

  def cfg(self, fn, bb):
    _, rs = self.expr.cfg(fn, bb)
    if isinstance(self.var, Var):
      if self.var.onHeap():
        bb.addStatement(rtl.StoreStack(rs, self.var.getheaploc(fn)))
      else:
        bb.addStatement(rtl.StoreLocal(self.var.getreg(), rs))
    elif isinstance(self.var, ModVar):
      rb = reg.temp()
      bb.addStatement(rtl.Move(rb, fn.varbasereg))
      bb.addStatement(rtl.StoreModVar(rs, rb, self.var.mod, self.var.name))
    elif isinstance(self.var, RecordVar):
      _, rv = self.var.record.cfg(fn, bb)
      bb.addStatement(rtl.StoreRecordVar(rs, rv, self.var.name))
    else:
      raise Exception("Unhandled var: %s" % self.var)
    return bb, None


class Const(Node):
  def __init__(self, name, value):
    self.name, self.value = name, value
  
  def fold(self):
    return self.value.fold()

  def cfg(self, fn, bb):
    return self.value.cfg(fn, bb)


class Pass(Node):
  def cfg(self, fn, bb):
    return bb, None


class RecordVar(Node):
  def __init__(self, name, record):
    self.name, self.record = name, record

  def cfg(self, fn, bb):
    rd = reg.temp()
    _, rs = self.record.cfg(fn, bb)
    bb.addStatement(rtl.LoadRecordVar(rs, self.name, rd))
    return bb, rd


class ModVar(Node):
  def __init__(self, name, mod, typ):
    self.name, self.mod, self.typ = name, mod, typ

  def cfg(self, fn, bb):
    rd = reg.temp()
    rb = reg.temp()
    bb.addStatement(rtl.Move(rb, fn.varbasereg))
    bb.addStatement(rtl.LoadModVar(self.mod, self.name, rd, rb))
    return bb, rd


class Var(Node):
  def __init__(self, name, typ=None, boxed=False):
    self.name, self.reg = name, None
    self.typ, self.boxed = typ, boxed
    self.heaploc = None

  def getreg(self):
    if not self.reg:
      self.reg = reg.temp()
    return self.reg

  def getheaploc(self, fn):
    if self.heaploc is None:
      self.heaploc = fn.frame.allocate("heap")
    return self.heaploc

  def onHeap(self):
    simpletypes = ["Int", "Bool", "Char"]
    return str(self.typ) not in simpletypes

  def cfg(self, fn, bb):
    if self.name in fn.args and fn.args.index(self.name) > 3:
      rd = reg.temp()
      bb.addStatement(rtl.LoadStack(rd, fn.frame.get("arg", fn.args.index(self.name)-4)))
      return bb, rd
    elif self.onHeap():
      if self.name in fn.args and self.heaploc is None:
        hl = self.getheaploc(fn)
        fn.bb.statements.insert(1, rtl.StoreStack(reg.r(fn.args.index(self.name)), hl))
      hl = self.getheaploc(fn)
      rd = reg.temp()
      bb.addStatement(rtl.LoadStack(rd, hl))
      return bb, rd
    elif self.name in fn.args and not fn.calls:
      # this function doesn't make any calls, we can clobber r[0-3]
      return bb, reg.r(fn.args.index(self.name))
    elif self.name in fn.args and not self.reg:
      # calls are made, so we need to copy arg to new register
      rs = self.getreg()
      rd = reg.temp()
      bb.addStatement(rtl.LoadLocal(rs, rd))
      fn.bb.statements.insert(1, rtl.Move(rs, reg.r(fn.args.index(self.name))))
      return bb, rd
    else:
      rs = self.getreg()
      rd = reg.temp()
      bb.addStatement(rtl.LoadLocal(rs, rd))
      return bb, rd


class Literal(Node):
  def __init__(self, value):
    self.value = value

  def cfg(self, fn, bb):
    rd = reg.temp()
    bb.addStatement(self.litcfg(fn, bb, rd))
    return bb, rd


class Integer(Literal):
  def litcfg(self, fn, bb, rd):
    return rtl.Integer(self.value, rd)


class Fixed(Literal):
  def litcfg(self, fn, bb, rd):
    return rtl.Integer(self.value, rd)

class Boolean(Literal):
  def litcfg(self, fn, bb, rd):
    return rtl.Integer(int(self.value), rd)


class Char(Literal):
  def litcfg(self, fn, bb, rd):
    return rtl.Integer(ord(self.value), rd)


class String(Literal):
  def litcfg(self, fn, bb, rd):
    return rtl.String(self.value, rd)


class ByteArray(Literal):
  def litcfg(self, fn, bb, rd):
    return rtl.ByteArray(self.value, rd)


class Tuple(Literal):
  pass


class Cast(Node):
  def __init__(self, expr, fromt, tot):
      self.expr, self.fromt, self.tot = expr, fromt, tot

  def cfg(self, fn, bb):
    rd = reg.temp()
    bb, r = self.expr.cfg(fn, bb)
    if self.fromt.name == "Int" and self.tot.name == "Fx16":
      bb.addStatement(rtl.Arith("<<", rd, r, None, 16))
    else:
      raise Exception("Unhandled cast %s->%s" % (self.fromt, self.tot))
    return bb, rd


class Box(Node):
  def __init__(self, expr, boxfn):
    self.expr, self.boxfn = expr, boxfn
  
  def cfg(self, fn, bb):
    c = Call(parser.Var._make((self.boxfn, ["GC"], 0)), [self.expr])
    return c.cfg(fn,bb)


class Call(Node):
  def __init__(self, func, args):
    self.func, self.args = func, args

  def cfg(self, fn, bb):
    atemps = []
    # load all boxed args first because if a boxed arg causes a gc we
    # don't want any heap reference in a register
    argpairs = sorted([(i, x) for i,x in enumerate(self.args)], 
                      key=lambda a: not isinstance(a[1], Box))
    # generate the args and save the index-reg pair
    for i, a in argpairs:
      bb, ar = a.cfg(fn,bb)
      atemps.append((i, ar))
    # do actual load regN load
    for i, a in atemps:
      if i < 4:
        bb.addStatement(rtl.SetupArg(i, a))
      else:
        bb.addStatement(rtl.StoreStack(a, fn.frame.get("callarg",i-4)))
    # call
    ret = reg.temp()
    pname = "".join(self.func.path)+"_" if len(self.func.path) else ""
    bb.addStatement(rtl.Call(pname+self.func.value, len(self.args)))
    bb.addStatement(rtl.Move(ret, reg.r(0)))
    return bb, ret


class Return(Node):
  def __init__(self, expr):
    self.expr = expr

  def cfg(self, fn, bb):
    bb, r = self.expr.cfg(fn, bb)
    bb.addStatement(rtl.Move(reg.r(0), r))
    bb.addStatement(rtl.Return())
    return bb, None


class Block(Node):
  def __init__(self, statements):
    self.statements = statements

  def cfg(self, fn, bb):
    curbb = bb
    for s in self.statements:
      curbb, r = s.cfg(fn, curbb)
    return curbb, None


class If(Node):
  def __init__(self, expr, tb, fb):
    self.expr, self.tb, self.fb = expr, tb, fb
    if not self.expr.isCmp():
      self.expr = Op("==", self.expr, Boolean(True), typecore.BaseTypes.T_Bool)

  def cfg(self, fn, bb):
    cmpop = self.expr.op if self.expr.isCmp() else None
    bb, re = self.expr.cfg(fn, bb, noResult=True)
    bbn = cfg.BasicBlock(name="endif")
    # handle trueblock
    bbt = cfg.BasicBlock(name="true")
    bbtn,_ = self.tb.cfg(fn, bbt)
    bbtn.addNext(bbn)
    # handle falseblock
    if self.fb:
      bbf = cfg.BasicBlock(name="false")
      bb.addNext(bbf)
      bb.addNext(bbt)
      bbf, _ = self.fb.cfg(fn, bbf)
      bbf.addNext(bbn)
    else:
      bb.addNext(bbn)
      bb.addNext(bbt)
    bb.addStatement(rtl.BranchIf(bbt.name, cmpop))
    return bbn, re


class For(Node):
  def __init__(self, var, init, limit, body, incr=1):
    self.var, self.body = var, body
    self.init = Assign(var, init)
    self.expr = Op(">=", var, limit, typecore.BaseTypes.T_Int)
    self.incr = Assign(var, Op("+", var, Integer(incr), typecore.BaseTypes.T_Int))

  def cfg(self, fn, bb):
    bbi, _ = self.init.cfg(fn, bb)
    bbe = cfg.BasicBlock(name="expr")
    bbe,_ = self.expr.cfg(fn, bbe, noResult=True)
    bbb = cfg.BasicBlock(name="body")
    bbn = cfg.BasicBlock(name="endfor")
    bbe.addStatement(rtl.BranchIf(bbn.name, ">="))
    bbb1,_ = self.body.cfg(fn, bbb)
    bbb2,_ = self.incr.cfg(fn, bbb1)
    bbi.addNext(bbe)
    bbe.addNext(bbb)
    bbe.addNext(bbn)
    bbb2.addNext(bbe)
    return bbn, None


class While(Node):
  def __init__(self, expr, body):
    self.expr, self.body = expr, body
    if not self.expr.isCmp():
      self.expr = Op("==", self.expr, Boolean(True), typecore.BaseTypes.T_Bool)

  def cfg(self, fn, bb):
    bbe = cfg.BasicBlock(name="expr")
    bb.addNext(bbe)
    forever = isinstance(self.expr, Boolean) and self.expr.value
    bbn = cfg.BasicBlock(name="endwhile")
    if not forever:
      self.expr.cfg(fn, bbe, noResult=True)
      bbe.addStatement(rtl.BranchIf(bbn.name, self.expr.op, invert=True))
    bbb = cfg.BasicBlock(name="body")
    bbe.addNext(bbb)
    bbb,_ = self.body.cfg(fn, bbb)
    if not forever:
      bbe.addNext(bbn)
    bbb.addNext(bbe)
    return bbn, None


class Function(Node):
  def __init__(self, name, args, body, env, calls, inmod, pos, pragmas=[]):
    self.name, self.args, self.body, self.env = name, args, body, env
    self.calls, self.inmod, self.pos = calls, inmod, pos
    self.pragmas = {}
    for p in pragmas:
        if "=" in p:
            k,v = p.split("=")
            self.pragmas[k] = v
        else:
            self.pragmas[p] = ""

  def cfg(self, frame):
    self.frame = frame
    self.bb = cfg.BasicBlock(name="entry")
    self.bb.addStatement(rtl.Prolog())
    # unbox boxed args
    for i, a in enumerate(self.args):
      t, v = self.env.lookup(a)
      if v.boxed:
        self.bb.statements.insert(1, rtl.Move(v.getreg(), reg.r(i)))
    for a in self.args:
      t, v = self.env.lookup(a)
      if v.boxed and typecore.boxingFunctionFor(t):
        fn = parser.Var._make(("unbox", ["GC"], 0))
        Assign(v, Call(fn, [v])).cfg(self, self.bb)
    # setup var base register
    self.varbasereg = reg.temp()
    self.bb.addStatement(rtl.LoadVarBase(self.varbasereg))
    # cfg body
    self.body.cfg(self, self.bb)
    return self.bb


class CFunction(Node):
  def __init__(self, name, args, cobj, env, calls, inmod, pos, pragmas=[]):
    self.name, self.args, self.cobj, self.env = name, args, cobj, env
    self.calls, self.inmod, self.pos = calls, inmod, pos
    self.pragmas = {}
    for p in pragmas:
        if "=" in p:
            k,v = p.split("=")
            self.pragmas[k] = v
        else:
            self.pragmas[p] = ""


class Struct(Node):
	def __init__(self, name, fields):
		self.name, self.fields = name, fields


class Module(Node):
  def __init__(self, name, env, body):
    self.name, self.env, self.body = name, env, body
