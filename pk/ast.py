from collections import namedtuple

class ParserException(Exception):
  def __init__(self, msg, loc, lineno, col, fn):
    Exception.__init__(self, msg)
    self.loc, self.lineno, self.col = loc, lineno, col
    self.fn = fn

Arg = namedtuple("Arg", "typ name pos")
Type = namedtuple("Type", "name args pos")
Block = namedtuple("Block", "statements")
Function = namedtuple("Function", "name args rtype body pragma pos")
CFunction = namedtuple("CFunction", "name args rtype cobj pragma pos")
Return = namedtuple("Return", "expr pos")
Assign = namedtuple("Assign", "var expr first pos")
Call = namedtuple("Call", "fn args pos")
For = namedtuple("For", "var expr body pos")
While = namedtuple("While", "expr body pos")
Op = namedtuple("Op", "op l r pos")
As = namedtuple("As", "expr typ pos")
Unary = namedtuple("Unary", "op expr pos")
If = namedtuple("If", "expr tblock eblock pos")
Var = namedtuple("Var", "value path pos")
Const = namedtuple("Const", "name value pos")
Int = namedtuple("Int", "value pos")
Float = namedtuple("Float", "value pos")
Bool = namedtuple("Bool", "value pos")
String = namedtuple("String", "value pos")
ByteArray = namedtuple("ByteArray", "value pos")
Char = namedtuple("Char", "value pos")
Tuple = namedtuple("Tuple", "value pos")
Module = namedtuple("Module", "name funcs filename pos")
Struct = namedtuple("Struct", "name fields pos")
Require = namedtuple("Require", "name pos")
Pass = namedtuple("Pass", "pos")
Pragma = namedtuple("Pragma", "value pos")

def pos_to_linecol(p, lines):
  cp = 0
  for lineno,l in enumerate(lines):
    if cp <= p <= (cp+len(l)):
      return lineno+1, p-cp+1
    cp += len(l)+1
  return lineno+1, len(l)

def exception_pos(e, lines):
  if e.lineno:
    return e.lineno, e.col, e.lineno, e.col
  elif e.loc:
    sl,sc = pos_to_linecol(e.loc, lines)
    return sl,sc,sl,sc
  else:
    return 1,1,1,1

def handle_compile_error(e, txt, printit=True):
  fnlines = txt.split("\n")
  line1, col1, line2,col2 = exception_pos(e, fnlines)
  msg = str(e)
  if e.fn != "":
    msg = "%s [line %d]: %s" % (e.fn, line1, msg)
  msg = "%s\n%s\n%s" % (fnlines[line1-1], (" "*(col1-1))+"^", msg)
  if printit:
    print
    print msg
  return msg
