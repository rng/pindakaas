import socket

class GDBRemote:
    def __init__(self, host="localhost", port=3333):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((host, port))
        self.s.send("+")

    def rledecode(self, data):
        i = 0
        edata = ""
        lastc = None
        while i < len(data):
            c = data[i]
            i += 1
            if c == "}":
                c = data[i]^0x20
                i += 1
            if c=="*":
                rep = ord(data[i])-28
                i += 1
                edata += lastc*(rep-1)
            else:
                edata += c
            lastc = c

        return edata

    def _bytedecode(self,data):
      if (len(data)%2) == 1:
        print "warning: byte-decode of odd-length data"
      b = []
      for i in range(len(data)/2):
        b.append(int(data[i*2:i*2+2],16))
      return b

    def _getc(self):
      while 1:
        c = self.s.recv(1)
        if len(c):
          return c

    def _putpkt(self, data):
        #print "putpkt:", data 
        edata = ""
        for d in data:
            if d in "#$}":
                edata += "}"
                edata += chr(ord(d)^0x20)
            else:
                edata += d
        check = sum(map(ord,edata)) % 256
        pkt = "$%s#%02x" % (edata, check)
        self.s.send(pkt)

    def _getpkt(self):
      c = self._getc()
      if c == "+":
        while c == "+":
          c = self._getc()
      else:
        pass
      if c != "$":
        raise Exception("no start: %s" % c)
      data = ""
      while c!="#":
        c = self._getc()
        data += c
      #print "data:", data[:-1]
      crc = self._getc()
      crc += self._getc()
      self.s.send("+")
      return data[:-1]

    def _simplecmd(self, cmd, name):
      self._putpkt(cmd)
      resp = self._getpkt()
      if resp != "OK":
        raise Exception("%s command failed (%s)" % (name, resp))

    def read(self, addr, count):
      self._putpkt("m%x,%x" % (addr, count))
      return self._bytedecode(self._getpkt())

    def write(self, addr, data):
      sbyte = "".join(["%02x" % x for x in data])
      self._simplecmd("M%x,%x:%s" % (addr, len(data), sbyte), "Write")

    def breakpoint_add(self, addr, size):
      self._simplecmd("Z0,%x,%x" % (addr, size), "Breakpoint add")
      #print "[dbg] Breakpoint add OK"
    
    def breakpoint_rem(self, addr, size):
      self._simplecmd("z0,%x,%x" % (addr, size), "Breakpoint rem")
      #print "[dbg] Breakpoint rem OK"

    def continueat(self, addr):
      self._putpkt("c%x" % addr)
      self.ctrl_c = False
      #print "[dbg] continue 0x%x" % addr
      while 1:
        try:
          r = self._getpkt()
          rcode = r[0]
          if rcode in "TWX":
            #print "[dbg] >", r
            break
          elif rcode == "O":
            msg = "".join(map(chr,self._bytedecode(r[1:])))
            #print "[dbg]>",msg,
        except KeyboardInterrupt:
          print "[dbg] Attempting a break"
          self.s.send(chr(0x3))
          resp = self._getpkt()
          self.ctrl_c = True
          break

    def getregs(self):
      self._putpkt("g")
      byte = self._bytedecode(self._getpkt())
      regs = []
      for i in range(len(byte)/4):
        regs.append(byte[i*4]+(byte[i*4+1]<<8)+(byte[i*4+2]<<16)+(byte[i*4+3]<<24))
      return regs
    
    def monitor(self, data):
      hexcmd = "".join(["%02x" % ord(c) for c in data])
      self._putpkt("qRcmd,%s" % hexcmd)
      while 1:
        r = self._getpkt()
        if r=="OK" or r=="":
          break
        if "thread" in r and r[0]=="T":
            pass # thread info packet (get this on first qemu start)
        else:
            msg = "".join(map(chr,self._bytedecode(r[1:])))
            if len(msg):
              #print "[dbg] |>",msg,
              pass

    def supported(self):
        self._putpkt("qSupported")
        return self._getpkt()

