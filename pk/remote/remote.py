import gdbrem
from pk.utils import bit_extract

class RemoteException(Exception):
  pass

class Remote:
    def __init__(self, transport):
      self.gdb = gdbrem.GDBRemote()
      self.gdb.monitor("halt")

    def targetinfo(self):
      chipids = {
        0x32 : "LM3S811",
        0x51 : "LM3S2110",
        0x55 : "LM3S2965",
        0x73 : "LM3S6965",
        0xB8 : "LM3S1968",
      }
      did1  = self.read(0x400fe004, 1)[0]
      ver, fam, partno = bit_extract(did1, (4,28), (4,24), (8,16))
      dc0 = self.read(0x400fe008, 1)[0]
      ramsize = round(((dc0>>16)&0xFFFF) / 4.0)
      flashsize = round((dc0&0xFFFF) / 4.0)*8
      print "Chip ID: [%X.%X] 0x%X (%s), RAM: %dKB Flash: %dKB" % (
              ver, fam, partno, chipids.get(partno, "?"), ramsize, flashsize)
      return int(ramsize*1024), int(flashsize*1024)

    def write(self, addr, data):
      byte = []
      for d in data:
        byte.append((d    )&0xFF)
        byte.append((d>>8 )&0xFF)
        byte.append((d>>16)&0xFF)
        byte.append((d>>24)&0xFF)
      self.gdb.write(addr, byte)

    def read(self, addr, count):
      b = self.gdb.read(addr, count*4)
      w = []
      for i in range(count):
        w.append(((b[i*4]) +
                  (b[i*4+1]<<8) +
                  (b[i*4+2]<<16) +
                  (b[i*4+3]<<24)))
      return w

    def regdump(self, regs=None):
      if not regs:
        regs = self.gdb.getregs()
      regnames = "r0 r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 r11 r12 sp lr pc".split()
      for j in range(4):
        for k in range(4):
          i = k*4+j
          print "%4s: %08x   " % (regnames[i], regs[i]),
        print
      print "cpsr: %08x" % (regs[-1])

    def call0(self, addr, fatalbreak):
      trampaddr = 0x20000100
      # trampoline: nop; mov r0, #0; ldr sp, [r0]; bl <addr>; nop; nop;
      tramp = [0x2000BF00, 0xD000F8D0, 0xD000F000, 0xBF00BF00]
      ofs = (addr - (trampaddr+8)) - 4
      imm11, imm10, i2, i1, s = bit_extract(ofs, (11,1), (10,12), (1,22), (1,23), (1,24))
      j1 = 1 - (i1^s)
      j2 = 1 - (i2^s)
      ja = (j1<<13)|(j2<<11)|(imm11)
      jb = (s<<10)|(imm10)
      tramp[2] |= (ja<<16 | jb)
      # write trampoline
      self.write(trampaddr, tramp)
      # set breakpoint at trampoline return
      self.gdb.breakpoint_add(trampaddr+0xC,2)
      if fatalbreak:
        self.gdb.breakpoint_add(fatalbreak,2)
      # run from trampoline start
      self.gdb.continueat(trampaddr+2)
      # get registers
      self.regs = self.gdb.getregs()
      r0, pc = self.regs[0], (self.regs[15]&0xFFFFFFFE)
      fatal = (pc == fatalbreak) or self.gdb.ctrl_c
      self.gdb.monitor("halt")
      self.gdb.breakpoint_rem(trampaddr+0xC,2)
      if fatalbreak:
        self.gdb.breakpoint_rem(fatalbreak,2)
      return r0, fatal

    def reset(self):
      self.gdb.monitor("reset")
      self.gdb.monitor("halt")
