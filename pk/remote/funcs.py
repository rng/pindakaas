from remote import Remote
from pk.dis.armv7 import DisassemblerARMv7
from pk.utils import bit_extract
import struct

class FuncManException(Exception):
  pass

class FuncState(object):
  def __init__(self, man, name, code, relocs, addr=None):
    self.man = man
    self.name, self.code, self.relocs = name, code, relocs
    self.address = addr

  def size(self):
    return len(self.code)*4

  def relocate(self):
    for rloc, rname, rtype in self.relocs:
      if rname not in self.man.funcs:
        raise FuncManException("Can't find function %s" % (rname))
      if not self.handleReloc(rname, self.man.funcs[rname].address):
        raise FuncManException("Can't relocate function %s" % rname)

  def disassemble(self):
    pass

  def __repr__(self):
    return "<F %s a:%s r:%s c:%s>" % (self.name, self.address, self.relocs, self.code)

class FuncStateARMv7(FuncState):

  def callAddress(self):
    return self.address + 1

  def handleReloc(self, name, addr):
    ok = False
    for rloc, rname, rtype in self.relocs:
      if name==rname:
        # standard arm branch address calculation: offset = target-(pc+4)
        ofs = (addr - (self.address + rloc)) - 4
        if ofs<0:
          ofs = (-ofs-1)^0xFFFFFFFF
        # instruction offset (rloc is in bytes, code is stored in words)
        baseidx = rloc/4
        # handle specific relocation type
        if rtype == "armrelfn":
            imm11, imm10, i2, i1, s = bit_extract(ofs, (11,1), (10,12), (1,22), (1,23), (1,24))
            j1 = 1 - (i1^s)
            j2 = 1 - (i2^s)
            if rloc%4==2:
              self.code[baseidx  ] |= ((s<<10)|(imm10))<<16
              self.code[baseidx+1] |= (j1<<13)|(j2<<11)|(imm11)
            else:
              self.code[baseidx] |= (j1<<29)|(j2<<27)|(imm11<<16)|(s<<10)|imm10
        elif rtype == "thumbrelfn":
            imm11 = (ofs>>1)&0x7FF
            if rloc%4==2:
                mask, shift = 0x0000FFFF, 16
            else:
                mask, shift = 0xFFFF0000, 0
            self.code[baseidx] &= mask
            self.code[baseidx] |= (0xE000|imm11) << shift
        else:
            raise FuncManException("Unknown relocation type '%s'" % rtype)
        ok = True
    return ok

class Machine(object):
  def __init__(self, rambase, trampofs, varofs, codeofs, arch):
    self.rambase = rambase
    self.trampstart = rambase + trampofs
    self.varstart = rambase + varofs
    self.codestart = rambase + codeofs
    self.ramsize = 0
    self.flashsize = 0
    self.arch = arch
    fshash = {
      "armv7" : FuncStateARMv7,
    }
    self.funcstate = fshash[arch]

  def setMemory(self, ramsize, flashsize):
      self.ramsize, self.flashsize = ramsize, flashsize

machines = {
    "stellaris" : Machine(0x20000000, 0x100, 0x110, 0x140, "armv7"),
}

class FuncManager(object):
  def __init__(self):
    self.rem = Remote(None)
    self.machine = machines["stellaris"]
    self.machine.setMemory(*self.rem.targetinfo())
    self.clear()

  def clear(self):
    self.funcs = {
      "__boot" : FuncStateARMv7(self, "__boot", [0]*4, [], self.machine.rambase+0x100),
      # FIXME: this isn't the correct vector table length
      "__vectors" : FuncStateARMv7(self, "__vectors", [0]*64, [], 0),
    }
    self.varbase = self.machine.varstart
    self.nextspace = self.freestart = self.machine.codestart

  def dump(self, fn=None):
    d = self.rem.read(self.freestart, self.nextspace-self.freestart)
    if not fn:
      print " ".join(["%08x" % di for di in d])
    else:
      f = file(fn,"wb")
      for w in d:
        f.write(struct.pack("I",w))

  def fatalHandler(self):
    if "__fatal" in self.funcs:
      return self.funcs["__fatal"].address+0xe
    else:
      return None

  def funcinfo(self):
    lastend = self.machine.trampstart
    for f in sorted(self.funcs.values(), key=lambda x: x.address):
      size = f.size()
      print "%-25s 0x%08x-0x%08x (%3d bytes)" % (
            f.name, f.address, f.address+size, size)
      lastend = f.address+size
    lastend -= self.machine.trampstart
    print "\n%d bytes used" % (lastend)

  def func_for_pc(self, pc):
    for f in sorted(self.funcs.values(), key=lambda x: x.address):
      size = f.size()
      if pc>=f.address and pc<(f.address+size):
        return f
    return None

  def jump(self, addr):
    print self.rem.call0(addr, self.fatalHandler())

  def read(self, addr, count, dump=True):
    mem = self.rem.read(addr, count)
    if dump:
      print "%08x:" % addr,
      for i, v in enumerate(mem):
        print "%08x" % v,
        if i%8 == 7:
          print "\n%08x:" % (addr+i*4),
      print
    return mem

  def disassemble(self, name, pc=-1):
    if name not in self.funcs:
      print "Function '%s' not found" % name
      return
    fn = self.funcs[name]
    code = self.rem.read(fn.address, fn.size()/4)
    def fnlookup(addr):
      fn = self.func_for_pc(addr)
      return fn.name if fn else None
    DisassemblerARMv7(fnlookup).disassemble(code, fn.address, pc)

  def backtrace(self):
    regs = self.rem.regs
    sp, lr = regs[13], regs[14]
    if (lr>>4) == 0xfffffff:
      # we're in an exception handler, so 8 values are pushed onto stack
      # http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0552a/Babefdjc.html
      sp += 8*4

    ssize = 0x20003000-sp
    stack = self.read(sp, ssize/4, False)
    print "------------"
    spi = 0

    while spi < len(stack):
      hdr = stack[spi]
      framesize = ((hdr>>8)&0xFF)+1
      if (spi+framesize-1) >= len(stack):
        # bad stack values, raw print and quit
        print "corrupt stack?"
        self.rem.regdump()
        print
        for i,v in enumerate(stack):
          print "%08x: %08x" % (sp+i*4,v)
        break
      faddr = stack[spi+framesize-1]
      func = self.func_for_pc(faddr)
      print "%08x" % (faddr),
      if not func:
        print "???"
      else:
        print "%s+0x%x" % (func.name, faddr-func.address)
      spi += framesize

  def reset(self):
    self.clear()
    self.rem.reset()

  def write(self, addr, data):
    self.rem.write(addr, data)

  def call(self, name, excp=False):
    calladdr = self.funcs[name].callAddress()
    res, fatal = self.rem.call0(calladdr, self.fatalHandler())
    if fatal:
      if excp:
        raise FuncManException("Fatal Exception 0x%02x" % res)
      print "Fatal Exception 0x%02x" % res
      regs = self.rem.regs
      pc = regs[15]&0xFFFFFFFE
      fn = self.func_for_pc(pc)
      if fn:
        print "\nAt:\n%08x %s+0x%x" % (pc, fn.name, pc-fn.address)
        self.disassemble(fn.name, pc)
      self.backtrace()
      return 0
    else:
      return res

  def refreshFunc(self, name):
    #print "* refresh", name
    addr = self.funcs[name].address
    self.write(addr, self.funcs[name].code)

  def push(self, name, code, relocs):
    fn = self.machine.funcstate(self, name, code, relocs)
    addressChanged = False
    # check if the function exists and needs to be replaced
    if name in self.funcs:
      oldfn = self.funcs[name]
      if oldfn.size() >= fn.size():
        # we have enough space to replace the func in its existing location
        fn.address = oldfn.address
      else:
        # not enough space, so create a new location
        fn.address = self.nextspace
        self.nextspace += fn.size()
        addressChanged = True
    else:
      # allocate new space
      fn.address = self.nextspace
      self.nextspace += fn.size()
    # if the address of this func changed, we need to update
    # functions that call it
    if addressChanged:
      for f in self.funcs.values():
        if f.handleReloc(fn.name, fn.address):
          self.refreshFunc(f.name)
    # relocate this function
    self.funcs[name] = fn
    return fn

  def vector(self, name, vectnum):
    #print "vector %s %d %08x" % (name, vectnum, self.funcs[name].address)
    vectword = self.funcs[name].address + 1
    tableofs = self.machine.rambase + (vectnum * 4)
    self.write(tableofs, [vectword])
