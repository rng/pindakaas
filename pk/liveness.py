import rtl
from cfg import BasicBlockVisitor

def liveness(cfg):
  worklist = cfg.all([])
  for bb in worklist:
    for s in bb.statements:
      s.livein = set()
      s.liveout = set()
  # apply liveness until we have no nodes left on worklist
  while len(worklist):
    changed = False
    bb = worklist.pop()
    for i, s in enumerate(bb.statements):
      prev_in, prev_out = s.livein.copy(), s.liveout.copy()
      # get next statements
      if i==(len(bb.statements)-1):
        succ = [bbn.statements[0] for bbn in bb.nexts]
      else:
        succ = [bb.statements[i+1]]
      # live in/out equations
      # http://en.wikipedia.org/wiki/Live_variable_analysis
      s.livein = set(s.cuses()) | (s.liveout - set(s.cdefs()))
      for sn in succ: 
        s.liveout |= sn.livein
      # check if there was a change
      if s.liveout != prev_out or s.livein != prev_in:
        changed = True
    # if this node changed, add it and its nexts to the worklist
    if changed:
      worklist.extend([bb]+[b for b in bb.nexts])
      

class DeadCode(BasicBlockVisitor):
  """ Deadcode elimination: remove useless moves and unused defs"""
  def visit(self, node):
    for i, s in enumerate(node.statements):
      nopit = False
      defs = s.cdefs()
      # if this is a rN<-rN move, it's useless
      if isinstance(s, (rtl.StoreLocal, rtl.LoadLocal, rtl.Move, rtl.SetupArg)) and s.rd==s.rs:
        nopit = True
      # if the temp defined isn't in liveout, it isn't used
      if len(defs) and defs[0] not in s.liveout and str(defs[0])[0]!="r":
        nopit = True
      # nop out instruction
      if nopit:
        node.statements[i] = rtl.Nop()
        self.changed = True


class CopyPropagate(BasicBlockVisitor):
  """ Awful copy propagation """
  def visit(self, node):
    livepairs = {}
    for s in node.statements:
      # for each of the current statement's uses, check against the live pairs
      for u in s.cuses():
        if u in livepairs:
          if isinstance(s, (rtl.StoreLocal, rtl.LoadLocal, rtl.Move, rtl.SetupArg)):
            s.rs = s.uregs[0] = livepairs[u]
            self.changed = True
          elif isinstance(s, (rtl.Cmp, rtl.Arith)):
            if s.rl == u:
              s.rl = s.uregs[0] = livepairs[u]
            elif s.rr == u:
              s.rr = s.uregs[1] = livepairs[u]
            self.changed = True
      # update live pairs based on this statement's defs
      for d in s.cdefs():
        dels = set()
        if d in livepairs:
          dels.add(d)
        for k,v in livepairs.items():
          if d==v:
            dels.add(k)
        for dl in dels:
          del livepairs[dl]
        if isinstance(s, (rtl.StoreLocal, rtl.LoadLocal, rtl.Move)):
          livepairs[d] = s.rs
      # if statement is a call remove any r[0-4] live since they could
      # be clobbered
      if isinstance(s, rtl.Call):
        dels = set()
        for k, v in livepairs.items():
          if v.num() < 4 and not v.isTemp():
            dels.add(k)
        for dl in dels:
          del livepairs[dl]
