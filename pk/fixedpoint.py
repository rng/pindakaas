import math
from typecore import TypeException

def float_to_fx(value, f=16, errpc=1):
    if math.floor(abs(value)) > (2**(32-f-1)):
        raise TypeException("Fixed-point value too large")
    fv = int(value*(2**f))
    fvr = fv/float(2**f)
    err = value-fvr
    perr = (err/value)*100
    if perr >= errpc:
        raise TypeException("Fixed-point value cannot be accurately represented")
    return fv

def fx_to_float(value, f=16):
    return value/float(2**f)
