
class RegException(Exception):
  pass

class Register(object):
  def __init__(self, name):
    self.name = name
    self.alloc = None if self.isTemp() else self

  def isTemp(self):
    return self.name.startswith("t")

  def isSpill(self):
    return self.alloc and self.alloc.name.startswith("s")

  def num(self):
    if self.alloc and self.alloc.name[0]=="s":
      raise RegException("trying to use spill reg %s" % self.alloc.name)
    return int(self.alloc.name[1:]) if self.alloc else int(self.name[1:])

  def rawnum(self):
    return int(self.alloc.name[1:]) if self.alloc else int(self.name[1:])

  def allocated(self):
    return self.alloc!=None

  def allocate(self, r):
    self.alloc = r

  def __repr__(self):
    return self.alloc.name if self.alloc else self.name


class Reg:
  def __init__(self):
    self.reset()

  def reset(self):
    self.tempn = 0
    self.spilln = 0
    self.concretes = {}

  def make(self,name):
    return Register(name)

  def r(self, n):
    if n not in self.concretes:
      self.concretes[n] = self.make("r%d" % n)
    return self.concretes[n]

  def spill(self, r):
    r.allocate(self.make("s%d" % self.spilln))
    self.spilln += 1

  def temp(self):
    self.tempn += 1
    return self.make("t%d" % (self.tempn-1))

reg = Reg()

