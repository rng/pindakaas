from reg import reg
import rtl

class Allocator(object):

  def calculate_ranges(self, seq):
    ranges = {}
    for i,s in enumerate(seq):
      if hasattr(s, "livein"):
        alllive = s.livein|s.liveout
        for sa in alllive:
          if str(sa)[0]!="r":
            if sa not in ranges:
              ranges[sa] = [i,i]
            else:
              ranges[sa][1] = i
    return ranges

  def allocate(self, seq, frame):
    """ Linear scan allocator
        http://en.wikipedia.org/wiki/Register_allocation """

    dus = self.calculate_ranges(seq)
    intervals = sorted([[k,v[0],v[1]] for k,v in dus.items()],key=lambda x: x[1])

    freeregs = [reg.r(i) for i in range(4,7)]

    allocation = []

    def addallocation(temp, t, reg):
      allocation.append((temp,t,reg))

    def choosespill(active):
      maxend = 0
      maxn = None
      for i,a in enumerate(active):
        if a[2]>maxend:
          maxend = a[2]
          maxn = i
      return maxn

    active = []
    # scan
    for i, interval in enumerate(intervals):
      itemp, istart, iend = interval
      #print "interval %d: %s %s-%s" % (i, itemp, istart,iend), active
      # 1. move active -> allocation
      apop = []
      for j, act in enumerate(active):
        atemp, areg, aend = act
        if aend <= istart:
          addallocation(atemp, "reg", areg)
          freeregs.append(areg)
          apop.append(j)
      nactive = []
      for j, a in enumerate(active):
        if j not in apop:
          nactive.append(a)
      active = nactive
      # 2. allocate current interval
      if len(freeregs):
        # allocate
        r = freeregs.pop(0)
        active.append((itemp, r, iend))
      else:
        # spill
        spillidx = choosespill(active)
        spill = active[spillidx] 
        spilltemp, spillreg, spillend = spill
        if spillend > iend:
          active.pop(spillidx)
          active.append((itemp, spillreg, iend))
          addallocation(spilltemp, "spill", istart)
        else:
          addallocation(itemp, "spill", istart)
    # allocate final actives
    for j, act in enumerate(active):
      atemp, areg, aend = act
      addallocation(atemp, "reg", areg)
      freeregs.append(areg)
    # allocate everything
    spills = {}
    an = 0
    for atemp, atype, areg in allocation:
      if atype=="reg":
        atemp.allocate(areg)
      else:
        reg.spill(atemp)

    return self.insert_spills(seq, frame)

  def insert_spills(self, seq, frame):
    # add spill handling instructions
    def add_load(rd, loc):
      newseq.append(rtl.LoadStack(rd, frame.get("spill", loc.rawnum())))
    def add_store(rs, loc):
      newseq.append(rtl.StoreStack(rs, frame.get("spill", loc.rawnum())))

    newseq = []
    for i,s in enumerate(seq):
      spills = any([r.isSpill() for r in s.defuses()])
      if spills:
        if isinstance(s, rtl.LoadLocal):
          rd = reg.r(7) if s.rd.isSpill() else s.rd
          if s.rs.isSpill():
            add_load(rd, s.rs)
          else:
            newseq.append(rtl.Move(rd, s.rs))
          if s.rd.isSpill():
            add_store(s.rs, s.rd)
        elif isinstance(s, rtl.StoreLocal):
          if s.rs.isSpill():
            add_load(reg.r(7), s.rs)
            s.rs = s.uregs[0] = reg.r(7)
          if s.rd.isSpill():
            add_store(s.rs, s.rd)
          else:
            newseq.append(rtl.StoreLocal(s.rd, s.rs))
        elif isinstance(s, (rtl.Cmp, rtl.Arith, rtl.SMult64)):
          post = False
          if s.rl.isSpill() and s.rr and s.rr.isSpill():
            # this is hacky FIXME
            rrnum = 6 if (s.rd and s.rd.num()!=6) else 5
            add_load(reg.r(7), s.rl)
            newseq.append(rtl.Move(reg.r(8), reg.r(rrnum)))
            add_load(reg.r(rrnum), s.rr)
            s.rl = s.uregs[0] = reg.r(7)
            s.rr = s.uregs[1] = reg.r(rrnum)
            post = True
          else:
            if s.rl.isSpill():
              add_load(reg.r(7), s.rl)
              s.rl = s.uregs[0] = reg.r(7)
            if s.rr and s.rr.isSpill():
              add_load(reg.r(7), s.rr)
              s.rr = s.uregs[1] = reg.r(7)
          newseq.append(s)
          if s.rd and s.rd.isSpill():
            add_store(reg.r(7), s.rd)
            s.rd = s.dregs[0] = reg.r(7)
          if post:
            newseq.append(rtl.Move(reg.r(rrnum), reg.r(8)))
        elif isinstance(s, (rtl.Integer, rtl.SetIf, rtl.Move, rtl.SetupArg, rtl.LoadModVar, rtl.LoadVarBase)):
          if s.rs and s.rs.isSpill():
            add_load(reg.r(7), s.rs)
            s.rs = s.uregs[0] = reg.r(7)
          newseq.append(s)
          if s.rd.isSpill():
            add_store(reg.r(7), s.rd)
            s.rd = s.dregs[0] = reg.r(7)
        elif isinstance(s, rtl.LoadStack):
          newseq.append(rtl.LoadStack(reg.r(7), s.loc))
          add_store(reg.r(7), s.rd)
        elif isinstance(s, rtl.StoreStack):
          add_load(reg.r(7), s.rs)
          newseq.append(rtl.StoreStack(reg.r(7), s.loc))
        else:
          raise Exception("Unhandled spill in statement: %s" % s)
      else:
        newseq.append(s)
    # calculate clobbers
    clobbers = set()
    for i,s in enumerate(newseq):
      clobbers |= set(map(str,s.defuses()))
    return newseq, (clobbers - set(["r0", "r1", "r2", "r3"]))
