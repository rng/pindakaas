# vim:set ts=2 sw=2 sts=2 et: #
import parser
import node
import fixedpoint
from typecore import (prune, occursInType, TypeVar, TypeOper, TypeRecord,
                      TypeException, BaseTypes, fresh, boxingFunctionFor,
                      isPolymorphic, TypeState)


def unify(t1, t2, ts, expr=None):
  a = prune(t1)
  b = prune(t2)
  if isinstance(a, TypeVar):
    if a != b:
      if occursInType(a, b):
        raise TypeException("Recursive unification", expr, ts.fn)
      a.instance = b
  elif isinstance(a, TypeOper) and isinstance(b, TypeVar):
    unify(b, a, ts, expr)
  elif isinstance(a, TypeOper) and isinstance(b, TypeOper):
    if (a.name != b.name or len(a.types) != len(b.types)):
      raise TypeException("Type mismatch %s, %s" % (a, b), expr, ts.fn)
    for p, q in zip(a.types, b.types):
      unify(p, q, ts, expr)
  else:
    raise TypeException("Unification error", expr, ts.fn)


class Environment(object):
  def __init__(self, name, parent=None, values=None, nodes=None):
    self.name = name
    self.values = values if values else {}
    self.nodes = nodes if nodes else {}
    self.parent = parent

  def insert(self, name, typ, node, ts=None, replace=False):
    if name in self.values and not replace:
      unify(self.values[name], typ, ts)
    else:
      self.values[name] = typ
      self.nodes[name] = node

  def lookup(self, name, ts=None, expr=None, nametype="variable", upto=""):
    if name not in self.values:
      if (not self.parent) or upto==self.name:
        raise TypeException("%s '%s' not found" % (nametype, name), expr, ts.fn if ts else ts)
      return self.parent.lookup(name, ts, expr, nametype)
    return self.values[name], self.nodes[name]

  def has(self, name, upto=""):
    try:
      self.lookup(name, upto=upto)
      return True
    except:
      return False

  def has0(self, name):
    return name in self.values

  def remove(self, name):
    del self.values[name]
    del self.nodes[name]

  def __repr__(self):
    return "<Env %s '%s' %s>" % (id(self), self.name, self.values)


def module_lookup(var, env, expr, ts, upto=""):
  def module_recurse(env, name, path):
    if not isinstance(env, Environment):
      raise TypeException("Invalid module", expr, ts.fn)
    if len(path):
      modt, mod = env.lookup(path[0], ts, expr, nametype="module")
      modt = prune(modt)
      if modt == BaseTypes.T_Module:
        return module_recurse(mod, name, path[1:])
      elif isinstance(modt, TypeOper) and modt.name in ts.modtypes:
        st = ts.modtypes[modt.name]
        if var.value not in st.names:
          raise TypeException("No field '%s' in type %s" % (var.value, modt.name), expr, ts.fn)
        fidx, ft = st.fieldInfo(var.value)
        _, stvar = env.lookup(var.path[0], ts)
        v = node.RecordVar(fidx+1, stvar)
        return ft, v
      else:
        raise TypeException("Invalid module type: %s" % modt, expr, ts.fn)
    else:
      return env.lookup(name, ts, expr, upto=upto)
  return module_recurse(env, var.value, var.path)


def type_call(e, env, ts, nongen):
  ts.calls += 1
  # get function type from environment
  if not isinstance(e.fn, parser.Var):
    raise TypeException("Invalid function", e.fn, ts.fn)
  v,vv = module_lookup(e.fn, env, e.fn, ts)
  if vv and not isinstance(vv, (node.Function, node.CFunction)):
      raise TypeException("First-class functions not supported", e.fn, ts.fn)
  # type arguments
  args = map(list,zip(*[type_expr(a, env, ts, nongen) for a in e.args]))
  argtypes, argexprs = [], []
  if len(args):
    argtypes, argexprs = args
  # unify function
  rt = TypeVar()
  fntype = TypeOper("Fn", argtypes+[rt])
  unify(fntype, fresh(v, nongen), ts, e.fn)
  # handle polymorphic arguments
  if isinstance(v, TypeVar):
    polys = [isPolymorphic(t) for t in fntype.types[:-1]]
  else:
    polys = [isPolymorphic(t) for t in v.types[:-1]]
  # handle boxed args
  if any(polys):
    for i, argtype in enumerate(argtypes):
      boxfn = boxingFunctionFor(argtype)
      if polys[i] and boxfn:
        argexprs[i] = node.Box(argexprs[i], boxfn)
  # generate call
  fname = e.fn
  if vv and vv.inmod != "":
    fname = parser.Var._make((e.fn.value, [vv.inmod], e.fn.pos))
  return rt, node.Call(fname, argexprs)


def type_bytearray(e, env, ts, nongen):
  bytevals = []
  for v in e.value:
    vn = v
    if isinstance(v, parser.Var):
      _,vv = module_lookup(v, env, v, ts)
      if not isinstance(vv, node.Const):
        raise TypeException("Byte array value '%s' must be const" % v.value, v, ts.fn)
      vn = vv.value
    if isinstance(vn, (parser.Int, node.Integer)) and (0 <= vn.value <= 255):
      bytevals.append(vn.value)
    else:
      raise TypeException("Byte array value invalid", v, ts.fn)
  return TypeOper("ByteArray", [BaseTypes.T_Int]), node.ByteArray(bytevals)


def type_as(e, env, ts, nongen):
  te, ee = type_expr(e.expr, env, ts, nongen)
  astype = ts.type_from_ast(e.typ, e)
  if "+unsafe_as" in ts.pragmas:
    return astype, ee
  else:
    valid_casts = [
      ("Char", "Int",  True),
      ("Int",  "Char", True),
      ("Int",  "Fx16", False),
    ]
    for expr_t, cast_t, implicit in valid_casts:
      if expr_t == te.name and cast_t == astype.name:
        return astype, (ee if implicit else node.Cast(ee, te, astype))
    raise TypeException("Cannot cast %s to %s" % (te, astype), e, ts.fn)


def type_expr(e, env, ts, nongen):
  if isinstance(e, parser.Op):
    tl, el = type_expr(e.l, env, ts, nongen)
    tr, er = type_expr(e.r, env, ts, nongen)
    unify(tl, tr, ts, e)
    valid_ops = {
        "Char" : ["+", "-"],
        "Int"  : ["+", "-", "*", "/", "%", ">>", "<<", "&", "|"],
        "Fx16" : ["+", "-", "*"],
        "Bool" : ["or", "and"],
    }
    nd = node.Op(e.op, el, er, tl)
    if e.op in ["==","!=","<",">","<=",">="]:
      return BaseTypes.T_Bool, nd
    tn = prune(tl)
    if isinstance(tn, TypeVar):
      raise TypeException("Can't perform primitive operation on polymorphic type", e, ts.fn)
    if e.op not in valid_ops.get(tn.name, []):
      raise TypeException("Invalid operation on %s" % tn.name, e, ts.fn)
    return tl, nd
  elif isinstance(e, parser.Unary):
    te, ee = type_expr(e.expr, env, ts, nongen)
    if e.op == "not":
      rt = BaseTypes.T_Bool
    elif e.op == "-" and prune(te) in [BaseTypes.T_Fx16, BaseTypes.T_Int]:
      rt = te
    else:
      rt = TypeVar()
    unify(te, rt, ts, e)
    return te, node.Unary(e.op, ee)
  elif isinstance(e, parser.As):
    return type_as(e, env, ts, nongen)
  elif isinstance(e, parser.Call):
    return type_call(e, env, ts, nongen)
  elif isinstance(e, parser.Var):
    vt, var = module_lookup(e, env, e, ts)
    if isinstance(var, Environment):
      raise TypeException("First-class environments not supported", e, ts.fn)
    return fresh(vt, nongen), var
  elif isinstance(e, parser.Bool):
    return BaseTypes.T_Bool, node.Boolean(e.value)
  elif isinstance(e, parser.Int):
    return BaseTypes.T_Int, node.Integer(e.value)
  elif isinstance(e, parser.Float):
    return BaseTypes.T_Fx16, node.Fixed(fixedpoint.float_to_fx(e.value, 16))
  elif isinstance(e, parser.String):
    return BaseTypes.T_String, node.String(e.value)
  elif isinstance(e, parser.Char):
    return BaseTypes.T_Char, node.Char(e.value)
  elif isinstance(e, parser.Tuple):
    ttypes, texprs = zip(*[type_expr(a, env, ts, nongen) for a in e.value])
    return TypeOper("Tuple", ttypes), node.Tuple(texprs)
  elif isinstance(e, parser.ByteArray):
    return type_bytearray(e, env, ts, nongen)
  else:
    raise Exception("unhandled expr %s" % str(e))


def type_if(i, env, ts, nongen):
  te, ee = type_expr(i.expr, env, ts, nongen)
  unify(te, BaseTypes.T_Bool, ts, i.expr)
  tb, texpr = type_block(i.tblock, env, ts, nongen)
  if i.eblock:
    fb, fexpr = type_block(i.eblock, env, ts, nongen)
    unify(tb, fb, ts, i)
  else:
    fexpr = None
  return tb, node.If(ee, texpr, fexpr)


def type_while(w, env, ts, nongen):
  te, ee = type_expr(w.expr, env, ts, nongen)
  unify(te, BaseTypes.T_Bool, ts, w.expr)
  tb, body = type_block(w.body, env, ts, nongen)
  return tb, node.While(ee, body)


def type_for(f, env, ts, nongen):
  var = node.Var(f.var, BaseTypes.T_Int)
  env.insert(f.var, BaseTypes.T_Int, var, ts, replace=True)
  if isinstance(f.expr, parser.Tuple) and len(f.expr.value) in [2,3]:
    te, exp = type_expr(f.expr, env, ts, nongen)
    (init, limit), incr = exp.value[:2], 1
    if len(f.expr.value)==3:
      incr = exp.value[2].value
    tblk, blk = type_block(f.body, env, ts, nongen)
    return BaseTypes.T_Int, node.For(var, init, limit, blk, incr)
  else:
    raise TypeException("Need a 2-tuple as for expression", f.expr, ts.fn)


def type_assign(a, env, ts, nongen):
  et, ee = type_expr(a.expr, env, ts, nongen)
  if a.first:
      var = node.Var(a.var.value, et)
  elif env.has(a.var.value):
    _,var = module_lookup(a.var, env, a, ts)
  else:
    raise TypeException("Variable %s not found" % (a.var.value), a, ts.fn)
  env.insert(a.var.value, et, var, ts)
  return et, node.Assign(var, ee)


def type_const(v, env, ts, nongen):
  et, ee = type_expr(v.value, env, ts, nongen)
  const = node.Const(v.name, ee)
  env.insert(v.name, et, const, ts)
  return et, const


def type_modvar(v, env, ts):
  if env.has(v.name):
    raise TypeException("Module variable %s already defined" % (v.name), v, ts.fn)
  ts.check_type(v.typ[0], v)
  vt = ts.types[v.typ[0]]
  assert ts.modname != ""
  var = node.ModVar(v.name, ts.modname, vt)
  env.insert(v.name, vt, var, ts)
  return vt, var


def type_return(r, env, ts, nongen):
  et, ee = type_expr(r.expr, env, ts, nongen)
  return et, node.Return(ee)


def type_block(blk, env, ts, nongen):
  env = Environment("block", env)
  rt = TypeVar() #BaseTypes.T_None
  nodes = []
  for stmt in blk.statements:
    if isinstance(stmt, parser.Assign):
      _,se = type_assign(stmt, env, ts, nongen)
    elif isinstance(stmt, parser.Const):
      _,se = type_const(stmt, env, ts, nongen)
    elif isinstance(stmt, parser.If):
      _,se = type_if(stmt, env, ts, nongen)
    elif isinstance(stmt, parser.For):
      _,se = type_for(stmt, env, ts, nongen)
    elif isinstance(stmt, parser.While):
      _,se = type_while(stmt, env, ts, nongen)
    elif isinstance(stmt, parser.Return):
      rt,se = type_return(stmt, env, ts, nongen)
      fntype,_ = env.lookup("%this", ts)
      unify(fntype.types[-1], rt, ts, stmt)
    elif isinstance(stmt, parser.Call):
      _,se = type_expr(stmt, env, ts, nongen)
    elif isinstance(stmt, parser.Pass):
      _,se = TypeVar(), node.Pass()
    else:
      raise Exception("unhandled statement %s" % str(stmt))
    nodes.append(se)
  return rt, node.Block(nodes)


def type_struct(struct, env, ts, nongen, inmod):
  ftypes, fnames, fnodes = make_fnargs(struct.fields, ts)
  stype = TypeRecord(struct.name,
                     [ftypes[n] for n in fnames],
                     [s.name for s in struct.fields])
  ts.modtypes[struct.name] = stype

  ctrenv = Environment("function", env, ftypes, fnodes)

  # Constructor type
  argvals = [ftypes[n] for n in fnames]
  contype = TypeOper("Fn", argvals+[stype])
  env.insert(struct.name, contype, node.Function(struct.name, fnames, node.Block([]), ctrenv, True, inmod, 0), ts)
  st = node.Struct(struct.name, fnodes)

  # Constructor function
  svar = node.Var("_tmp", st)
  body = [node.Assign(svar, node.Call(
                              parser.Var._make(("_allocate", ["GC"], 0)), [
                                  node.Integer(1+len(struct.fields)),
                                  node.Integer(0),
                                  node.Integer(4)
                              ]))]
  # set record header
  body.append(node.Assign(node.RecordVar(0, svar), node.Integer(stype.header())))
  # set fields
  for i, a in enumerate(fnames):
    fidx,_ = stype.fieldInfo(a)
    body.append(node.Assign(node.RecordVar(fidx+1, svar), fnodes[a]))
  body.append(node.Return(svar))

  ctr = node.Function(st.name, fnames, node.Block(body), ctrenv, True, inmod, 0)
  return stype, st, ctr


def make_fnargs(args, ts):
  ah, an, ad = {}, [], {}
  for a in args:
    if a.name in ah:
      raise TypeException("'%s' redeclared" % a.name, a, ts.fn)
    ah[a.name] = ts.type_from_ast(a.typ, a, typevarsAllowed=True)
    an.append(a.name)
    ad[a.name] = node.Var(a.name, ah[a.name], isPolymorphic(ah[a.name]))
  return ah, an, ad


def type_function(fn, env, ts, nongen, inmod):
  ts.func()
  argtypes, argnames, argnodes = make_fnargs(fn.args, ts)
  argvals = [argtypes[n] for n in argnames]
  rettype = ts.type_from_ast(fn.rtype, fn, typevarsAllowed=True) if fn.rtype else BaseTypes.T_None
  fntype = TypeOper("Fn", argvals+[rettype])
  fnenv = Environment("function", env, argtypes, argnodes)
  ts.pragmas = fn.pragma.value if fn.pragma else []
  fnn = node.Function(fn.name, argnames, node.Block([]), fnenv, True, inmod, fn.pos, ts.pragmas)
  env.insert(fn.name, fntype, fnn, ts, replace=True)
  env.insert("%this", fntype, fnn, ts, replace=True)
  rt, be = type_block(fn.body, fnenv, ts, nongen|set(fntype.types))
  unify(fntype.types[-1], rt, ts, fn)
  if isinstance(fntype.types[-1], TypeOper) and fntype.types[-1].name == "None":
    be.statements.append(node.Return(node.Integer(0)))
  pr = prune(fntype.types[-1])
  if isinstance(pr, TypeOper) and pr.name == "Fn":
    raise TypeException("First-class functions not supported", fn, ts.fn)
  env.remove("%this")
  for a, at in argtypes.items():
    fnenv.insert(a, at, node.Var(a, at, isPolymorphic(at)), ts, replace=True)
  return fntype, node.Function(fn.name, argnames, be, fnenv, ts.calls, inmod, fn.pos, ts.pragmas)


def type_cfunction(fn, env, ts, nongen, inmod):
  ts.func()
  argtypes, argnames, argnodes = make_fnargs(fn.args, ts)
  argvals = [argtypes[n] for n in argnames]
  rettype = ts.type_from_ast(fn.rtype, fn, typevarsAllowed=True) if fn.rtype else BaseTypes.T_None
  fntype = TypeOper("Fn", argvals+[rettype])
  fnenv = Environment("function", env, argtypes, argnodes)
  ts.pragmas = fn.pragma.value if fn.pragma else []
  fnn = node.CFunction(fn.name, argnames, fn.cobj, fnenv, True, inmod, fn.pos, ts.pragmas)
  env.insert(fn.name, fntype, fnn, ts, replace=True)
  for a, at in argtypes.items():
    fnenv.insert(a, at, node.Var(a, at, isPolymorphic(at)), ts, replace=True)
  return fntype, node.CFunction(fn.name, argnames, fn.cobj, fnenv, ts.calls, inmod, fn.pos, ts.pragmas)


def type_module(mod, env, filename=None):
  if not filename:
    filename = mod.filename
  ts = TypeState(filename, mod.name)
  modtype = BaseTypes.T_Module
  if mod.name == "":
    modenv = env
  else:
    modenv = Environment("module", parent=env)
    env.insert(mod.name, modtype, modenv, ts)
  ret = []
  nongen = set()
  for f in mod.funcs:
    if isinstance(f, parser.Function):
      if modenv.has0(f.name):
        raise TypeException("Module function %s already defined" % f.name, f, ts.fn)
      fnn = node.Function(f.name, [], node.Block([]), env, True, mod.name, f.pos)
      modenv.insert(f.name, TypeVar(), fnn, ts)
  for f in mod.funcs:
    if isinstance(f, parser.Function):
      ret.append(type_function(f, modenv, ts, nongen, mod.name))
    elif isinstance(f, parser.CFunction):
      ret.append(type_cfunction(f, modenv, ts, nongen, mod.name))
    elif isinstance(f, parser.Module):
      ret.append(type_module(f, modenv, filename))
    elif isinstance(f, parser.Require):
      pass
    elif isinstance(f, parser.Arg):
      ret.append(type_modvar(f, modenv, ts))
    elif isinstance(f, parser.Const):
      ret.append(type_const(f, modenv, ts, nongen))
    elif isinstance(f, parser.Struct):
      stype, st, ctr = type_struct(f, modenv, ts, nongen, mod.name)
      ret.append((stype, ctr))
      ret.append((stype, st))
    else:
      raise Exception("Unhandled ast %s" % repr(f))
  ret = node.Module(mod.name, modenv, ret)
  return modtype, ret
