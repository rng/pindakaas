#include <stdint.h>

int main();

void nmi() {
  while (1);
}

void hardfault() {
  while (1);
}

void memmanage() {
  while (1);
}

void busfault() {
  while (1);
}

void usagefault() {
  while (1);
}

void svc() {
  while (1);
}

void debugmon() {
  while (1);
}

__attribute__ ((section(".vectors")))
void *vectors[] = {
	0x20000000 + 0x3000,
	main,
	nmi,
	hardfault,
  memmanage,
  busfault,
  usagefault,
  0,
  0,
  0,
  0,
  svc,
  debugmon,
  0,
};


int main() {
    while (1) {
    }
}
