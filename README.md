Intro:
------

Pindakaas is an interactive language for ARM microcontrollers. It's compiled, statically-typed (with type-inference), and garbage-collected. It currently generates ARMv7 code and runs on QEMU and Stellaris (TI) LM3Sxxxx chips.

Example:
--------

	Chip ID: [1.0] 0x73 (LM3S6965), RAM: 64KB Flash: 256KB
	
	Pindakaas REPL. Type '/help' for info.
	>> def fact(n : Int) : Int
	..   if n > 0 then
	..     return n * fact(n - 1)
	..   else
	..     return 1
	..   end
	.. end
	>> fact(4)
	24
	>> fact(6)
	720
	>> /d fact
	  20000140: b570      push  r4, r5, r6, lr
	  20000142: b081      sub   sp, #4
	  20000144: f2404742  mov   r7, #442
	  20000148: 9700      str   r7, [sp, #0]
	  2000014a: 4604      mov   r4, r0
	  2000014c: 2800      cmp   r0, #0
	  2000014e: f3008004  bge   2000015a
	  20000152: 2501      mov   r5, #1
	  20000154: 4628      mov   r0, r5
	  20000156: b001      add   sp, #4
	  20000158: bd70      pop   r4, r5, r6, pc
	  2000015a: 1e66      sub   r6, r4, #1
	  2000015c: 4630      mov   r0, r6
	  2000015e: f7ffffef  bl    fact (20000140)
	  20000162: fb04f500  mul   r5, r4, r0
	  20000166: 4628      mov   r0, r5
	  20000168: b001      add   sp, #4
	  2000016a: bd70      pop   r4, r5, r6, pc
	>> 

Dependencies:
-------------

* python
* python-ply
* QEMU
* libelf

On Ubuntu:

	sudo apt-get install python-ply qemu libelf-dev openocd

Usage:
------

Compile and run stub with QEMU:

	make run_gdbcore

Or, to use real hardware:

	make openocd

Compile parser:

	make parser

Run tests:

	make test

Start Pindakaas REPL:

	./pindakaas.py
