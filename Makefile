#MACHINE = lm3s811evb
MACHINE = lm3s6965evb
QEMUFLAGS = -M $(MACHINE) -gdb tcp::3333

all: gdbcore.bin

gdbcore.bin: core/gdbcore.pk
	./pkc -b -o $@ $<

run_gdbcore: gdbcore.bin
	qemu-system-arm $(QEMUFLAGS) -kernel $<

parser:
	python -c "import pk.parser as p; p.parseRepl('require a'); p.parseProgram('require a')"

test:
	./runtests.py

openocd:
	openocd -f tools/node_reva.cfg

sloc:
	find . -name "*.py" | grep -v "old" | grep -v "tab.py" | xargs sloccount

wc:
	find . -name "*.py" | grep -v "old" | grep -v "tab.py" | xargs wc -l | sort -n

clean:
	rm -rf *.s *.elf *.pyc \
           test/*.bin test/*.pkc \
           gdbcore.bin \
           fulltab.* repltab.* parser.out

time_compileall:
	time ./pkc -b test/test*.rb

time_testall:
	time ./runtests.py

.PHONY: dump_gdbcore clean sloc wc test openocd parser

